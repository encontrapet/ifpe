<?php 
/**
 * 
 * 
*	@autor Flávio Silva Brandão <flavio@afixo.com.br>
*	@version 03-06-2021 as 09:00:30
*
* 
* */



/*
 * habilitando o uso de sessoes
 * */

ob_start();
session_start();

/*
 * verificando se o usuario esta logado senao redireciona
 * */
if (!$_SESSION['ENCONTRAPET_codigo']){
	header("Location: ../splash");
}

# 
# REQUIRES OBRIGATORIOS
#


require('../classes/gestor.php');

#
# INSTANCIA DE OBJETOS OBRIGATORIAS
#


$util = new Util();
$gestor   = new Gestor();    

#
# TRATAMENTO DE INJECTION
#

$_POST = $util->validaParametro($_POST);
$_GET  = $util->validaParametro($_GET);


/*
 * 
 * reuperar os pets cadastrados anteriormente 1,3,6 */
 
$sql = "SELECT * FROM pets WHERE id_pessoas = " . $_SESSION['ENCONTRAPET_codigo'] . " AND id_status IN (1,3,6) ORDER BY data_cadastro DESC";

													
$statement = $gestor->db->db->prepare($sql);
//echo $sql;
//die;
$statement->execute();
$objetoPets = $statement->fetchAll(PDO::FETCH_OBJ);


//var_dump($objetoPets);
//die;
?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
  <link rel="icon" type="image/png" href="../assets/img/favicon.png">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    EncontraPet - Início
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <?php
  require("../inc/css.php");
  ?>
</head>

<body class="text-center" style=''>


              
       
              

            <div class="" style='padding:0'>      
				
				<?php
				require("../inc/navegacao.php")
				?>
            

              <!-- Nav tabs -->
              <div style="">
				  
				       
				  

                <div class="" style='padding-left:3px; padding-right:3px; padding-top:5px; '>
                  <!-- Tab panes -->
                  <div class="tab-content text-left">

					  
                    <div class="tab-pane active" id="pets" role="tabpanel">
						<div class='container-fluid' style='padding-bottom:20px; padding-top:20px'>
							<div class="row">
								<div class="col-12">
									
									<h5 class="" style=''>Pets cadastrados</h5>               
								</div>    
							</div>	
							
						<?php
						if ($objetoPets){
							?>
							<div class='card-columns' style='padding-bottom:0; margin-bottom:0'>
							<?php
							foreach($objetoPets as $obj){
								
								
								if ($obj->id_status == 1){
									$dataHora = $gestor->retornarMovimentacoesPets(array($obj->id,1),array('id_pets','id_status'),'movimentacoes_pets','id','DESC',1)->data_hora;
								} elseif($obj->id_status == 3){
									$dataHora = $gestor->retornarMovimentacoesPets(array($obj->id,3),array('id_pets','id_status'),'movimentacoes_pets','id','DESC',1)->data_hora;
								} elseif($obj->id_status == 6){
									$dataHora = $gestor->retornarMovimentacoesPets(array($obj->id,6),array('id_pets','id_status'),'movimentacoes_pets','id','DESC',1)->data_hora;

								}
								
								$imagem = $gestor->retornarFotos($obj->id,'id_pets','fotos','id','DESC',1)->caminho;
								
								
								
								$objetoInteracoesPets = $gestor->retornarInteracoesPets($obj->id,'id_pets','interacoes_pets','id','DESC');
								if ($objetoInteracoesPets){
									if (@count($objetoInteracoesPets) < 2){
										$objetoInteracoesPets = array($objetoInteracoesPets);
									}
								}
																
								$sql = "SELECT id_pessoas FROM dialogos_pessoas_pets WHERE id_pets = " . $obj->id . " AND id_pessoas <> " . $_SESSION['ENCONTRAPET_codigo'] . " GROUP BY id_pessoas";

													
								$statement = $gestor->db->db->prepare($sql);
								//echo $sql;
								//die;
								$statement->execute();
								$objetoConversasPets = $statement->fetchAll(PDO::FETCH_OBJ);

								if (!$imagem){
									if ($obj->id_especies == 1){ // cachorros
										$imagem = '../assets/img/pets/black-dog.png'; 
									} else {
										$imagem = '../assets/img/pets/black-cat.png'; 
									}
									
								} else {
									
									$imagem = '../uploads/pet/' . $imagem;
								}
									
								if ($dataHora){
									$dataHora= $util->retornaData($dataHora,'br');
								}
								?>
							
								<div class='card'  style='padding-bottom:5px;  padding-top:15px;  margin-bottom:10px;  background:rgba(233, 94, 56,0.4); border:0; box-shadow:none'>	
									
										
										
									<img src='<?=$imagem?>' style='width:94%; display:block; margin:0 auto; margin-bottom:10px'/>
									<div class='container' style=''>
										<div class='row' style='padding-bottom:10px'>
											<div class='col-12'>
												<small style='float:right'>Cadastrado(a) em: 
												<?php								
												
													echo $dataHora;
																				
												?>
												</small>
												<b><?=$gestor->retornarStatus($obj->id_status)->status?></b><br />
												<?php								
												if ($obj->sexo != "Não sei"){
													echo $obj->sexo;
												}								
												?>
											</div>
										</div>
									</div>
									
									
									<div class='container' style=''>
										<div class='row' style=''>
											
											<div class='col-2' style='padding-right:0; background:rgb(233, 94, 56)'>
												<a href='editar/?pet=<?=$obj->id?>' class='btn btn-primary col-2' style='width:100%; padding-left:10px '><i class='fa fa-edit'></i></a>									
											
											</div>
											<div class='col' style='padding:0; background:rgb(233, 94, 56)'>
												<a class="btn btn-primary"  onclick="$('#collapseConversa<?=$obj->id?>').collapse('hide')" style='width:100%;' data-toggle="collapse" href="#collapse<?=$obj->id?>" role="button" aria-expanded="false" aria-controls="collapse<?=$obj->id?>">
													<i class='fa fa-heart'></i>
													<?php
													
													if ($objetoInteracoesPets){
														if (count($objetoInteracoesPets) == 1 ){
															echo	count($objetoInteracoesPets) . " Interação";
														} else { 
														
															echo count($objetoInteracoesPets) . " Interações";					
														}
													} else {
														echo "0 Interações";
													}
													?>	
												</a>
											</div>
											<div class='col' style='margin:0; background:rgb(233, 94, 56)'>
												<a class="btn btn-primary " onclick="$('#collapse<?=$obj->id?>').collapse('hide')"  style='width:100%; padding-left:30px; padding-right:30px' data-toggle="collapse" href="#collapseConversa<?=$obj->id?>" role="button" aria-expanded="false" aria-controls="collapseConversa<?=$obj->id?>">
													<i class="far fa-comments"></i>
													<?php
													
													if ($objetoConversasPets){
														if (count($objetoConversasPets) == 1 ){
															echo	count($objetoConversasPets) . " Conversa";
														} else { 
														
															echo count($objetoConversasPets) . " Conversas";					
														}
													} else {
														echo "0 Conversas";
													}
													?>	
												</a>
											</div>
											
										</div>	
									</div>	
									<div class="collapse" id="collapse<?=$obj->id?>" style='opacity:0.8'>
										<div class="card card-body">
											
											
											
											
											
											
													<?php
													if ($objetoInteracoesPets){
														foreach($objetoInteracoesPets as $oo){
															
															
															$objPets = $gestor->retornarPets($oo->id_pets);
															$objPessoas = $gestor->retornarPessoas($oo->id_pessoas);
															
															$dataHora = $oo->data_hora;
															
															
																
															if ($dataHora){
																$dataHora= $util->retornaData($dataHora,'br');
															}
														?>
														
															<div class='row'>
																
																<div class='col-12 text-left' style='padding-left:0; '>
																	
																	
																<b><?=$objPessoas->nome?></b><br><?=$objPessoas->telefone?>
																<br />
																<?php								
																
																	echo '<small>' . $dataHora . ' às ' . substr($oo->data_hora,10) . '</small>';
																								
																?>
																<span style='float:right'>
																	
																	<a href='javascript:void(0);' onclick="cancelaInteressePet('<?=$oo->id_pets?>', '<?=$objPessoas->id?>')" class='rounded' style='padding:10px; background:#ddd'><i class='fa fa-ban' style='color:red'></i> Remover</a>
																	<a href='javascript:void(0);' onclick="adotaPet('<?=$oo->id_pets?>', '<?=$objPessoas->id?>')" class='rounded' style='padding:10px; background:#ddd'><i class='fa fa-check' style='color:green'></i> Aceitar</a>
																	
																	</span>
																</div>
																<div class='col-4'>
																
																</div>
																<div class='col-8 text-left'>

																								
																</div>
																<div class='col-12'>
																	<hr />
																</div>
															</div>
														
														
														
														<?php
														}
													} else {
													?>
													<p>Nenhuma interação até o momento.</p>
													<?php						
													}
													?>												
											
											
											
											
										</div>
									</div>	

									<div class="collapse" id="collapseConversa<?=$obj->id?>" style='opacity:0.8'>
										<div class="card card-body">
											
											
											
											
											
											
													<?php
													if ($objetoConversasPets){
														foreach($objetoConversasPets as $oo){
															
															
															$objPessoas = $gestor->retornarPessoas($oo->id_pessoas);
															
														?>
														
															<div class='row'>
																
																<div class='col-12 text-left' style='padding-left:0; '>
																	
																	
																<b><?=$objPessoas->nome?></b><br><?=$objPessoas->telefone?>
																<br />
																
																<span style='float:right'>
																	
																	
																	<a href='javascript:void(0);' onclick="conversa('<?=$obj->id?>', '<?=$objPessoas->id?>')" class='rounded' style='padding:10px; background:#ddd'><i class='fa fa-comments' style='color:green'></i> Conversar</a>
																	
																	</span>
																</div>
																<div class='col-4'>
																
																</div>
																<div class='col-8 text-left'>

																								
																</div>
																<div class='col-12'>
																	<hr />
																</div>
															</div>
															<form enctype="text/plain" method="get" action="../adotar/mensagem" id='formPrincipal' name='formPrincipal'>																									
																<input type='hidden' id='pet' name='pet' value='' />
																<input type='hidden' id='interessado' name='interessado' value='' />
															</form>
														
														<?php
														}
													} else {
													?>
													<p>Nenhuma conversa até o momento.</p>
													<?php						
													}
													?>												
											
											
											
											
										</div>
									</div>	


									</div>

							
							
							
							<?php
							}
							?>
							</div>
						<?php
						} else {
						?>
						<p>Nenhum pet cadastrado até o momento.</p>
						<?php						
						}
						?>
						
						</div>
					</div>

                   
                    
                  </div>
                  
						               
						  
                </div>




				<?php
				require("../inc/rodape.php");
				?>  



                
              </div>
			
			
            
            
            </div>
         
       
                       
              
                     
              
              
           </div>
         

 
  <!--   Core JS Files   -->
  <?php
 require("../inc/scripts.php") ;
  ?>
  <script>
	function conversa(pet, pessoa){
	
		$('#pet').val(pet);
		$('#interessado').val(pessoa);
		$('#formPrincipal').submit();
		//alert(pet + ' - ' + pessoa)
		//document.formPrincipal.submit();

	}

	function adotaPet(pet, pessoa){
	
					bootbox.hideAll()
					var dialog = bootbox.dialog({ 
						title: "Confirmação", 
						message: 'Esta pessoa irá adotar o Pet?',
						buttons: {
							
							ok: {
							  label: "Sim",
							  className: "btn-primary",
							  callback: function() {

								
									$.post('process.php?operacao=adotar&pet='+pet+'&pretendente='+pessoa,
										
										function(data){					   
											//alert(data);	
											if(data > 0){ // 1 ou maior 
															
												document.location.href = '../pets'							
															
											} else { //erro
															
												tituloMensagem = 'Erro:';
															
												tipo = 'erro';
															
												mensagem = 'Não foi possível realizar a operação.';
															
													
													bootbox.hideAll()
													var dialog = bootbox.dialog({ 
														title: tituloMensagem, 
														message: data,
														buttons: {
															
															ok: {
															label: "Ok",
															className: "btn-primary",
															callback: function() {
																dialog.modal('hide');
															}
															}
														},					
														onEscape: function() {
															dialog.modal('hide');
														}		  
															
													});
											}
															
											
															
										});



							  }
							},
							ok2: {
							  label: "Não",
							  className: "btn-primary",
							  callback: function() {
								dialog.modal('hide');
							  }
							}
						},					
						onEscape: function() {
							dialog.modal('hide');
						}		  
							
					});		
	}

	function cancelaInteressePet(pet, pessoa){
					bootbox.hideAll()
					var dialog = bootbox.dialog({ 
						title: "Confirmação", 
						message: 'Remover esta pessoa da lista de pretendentes a adoção?',
						buttons: {
							
								ok: {
				label: "Sim",
				className: "btn-primary",
				callback: function() {


					$.post('process.php?operacao=excluir&pet='+pet+'&pretendente='+pessoa,
					
					function(data){					   
						//alert(data);	
						if(data > 0){ // 1 ou maior 
										
							document.location.href = '../pets'							
										
						} else { //erro
										
							tituloMensagem = 'Erro:';
										
							tipo = 'erro';
										
							mensagem = 'Não foi possível excluir a mensagem.';
										
								
								bootbox.hideAll()
								var dialog = bootbox.dialog({ 
									title: tituloMensagem, 
									message: mensagem,
									buttons: {
										
										ok: {
										label: "Ok",
										className: "btn-primary",
										callback: function() {
											dialog.modal('hide');
										}
										}
									},					
									onEscape: function() {
										dialog.modal('hide');
									}		  
										
								});
						}
										
						
										
					});




				}
				},
				ok2: {
				label: "Não",
				className: "btn-primary",
				callback: function() {
					dialog.modal('hide');
				}
				}
			},					
			onEscape: function() {
				dialog.modal('hide');
			}		  
				
		});		
	}

	$('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
	  var url = $(this).attr("href"); // the remote url for content
	  var target = $(this).data("target"); // the target pane
	  var tab = $(this); // this tab
	  
	  // ajax load from data-url
	  $(target).load(url,function(result){      
		tab.tab('show');
	  });
	});

	// initially activate the first tab..
	$('#tabIni').click();  
  </script>  
</body>

</html>
