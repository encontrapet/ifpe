<?php 
header("content-type:application/json");
/**
*	@autor Flávio Silva Brandão <flavio@afixo.com.br>
*	@version 24-11-2020 as 09:04:28
*/

require('../../classes/gestor.php');

$util 	= new Util();
$gestor = new Gestor();

/* verificacao para direcionamento de get ou post */
if (!$_POST){
	
	extract($_GET);

	if (!@$id){ //  se não foi informado a quem buscar, buscar todos

		$objeto = $gestor->retornarInteracoesPets('');

		if ($objeto){
			if (count($objeto) < 2){
				$objeto = array($objeto);
			}
		}

	} else { // buscar a quem foi informado

		$objeto = $gestor->retornarInteracoesPets($id);
	}

	//print var_dump($objeto);
	print json_encode($objeto);

} else {

	extract($_POST);	

	@$objeto = array(array('id'=>$id,'id_pets'=>$id_pets,'id_pessoas'=>$id_pessoas,'data_hora'=>date("Y-m-d H:i:s")));

	switch (@$operacao) {
		case 'inserir':	

			$obj = $gestor->inserirInteracoesPets($objeto);		
			if ($obj){
				print($obj);
			} else {
				print(false);
			}	

		break;
		
		case 'editar':

			$obj = $gestor->editarInteracoesPets($objeto);		
			if ($obj){
				print(true);
			} else {
				print(false);
			}	

		break;
		
		case 'excluir':
			
			$obj = $gestor->excluirInteracoesPets($id);
			if ($obj){
				print(true);
			} else {
				print(false);
			}	
		break;

	}
}
?>