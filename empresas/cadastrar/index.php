<?php
ob_start();
session_start();
/**
*
*	@autor Flávio Silva Brandão <flavio@afixo.com.br>
*	@version 26-05-2021 as 08:56:55
*/


# 
# REQUIRES OBRIGATORIOS
#

require('../../classes/gestor.php');

$util = new Util();
$gestor   = new Gestor();

#
# TRATAMENTO DE INJECTION
#

$_POST = $util->validaParametro($_POST);

extract($_POST);
?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="../../assets/img/apple-icon.png">
  <link rel="icon" type="image/png" href="../../assets/img/favicon.png">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    EncontraPet - Empresas e ONGs - Cadastrar
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <?php
  require("../../inc/css2.php");
  ?>
</head>

<body style="padding-top:20px">


	<div class="col-12 col-lg-4 offset-lg-4" data-background-color="orange" style='padding-top:30px; padding-bottom:30px; '>
		<form class="form" method="" action="" id='formPrincipal' name='formPrincipal'>
		  
		  
		  				<div class="row">
							<div class="col-12 text-center">
								<img src='../../assets/img/logo.png' width='200px' />
								<h2 class="">EncontraPet</h2>
								<h3 class="card-title title-up" style='width:100%; text-align:center'>Informe os dados para Cadastro</h3>               
							</div>    
						</div>				                  
						<div class="row">
							<div class="col-12">
												 
												 

								  <div class="input-group no-border">
									<div class="input-group-prepend">
									  <span class="input-group-text">
										<i class="now-ui-icons users_circle-08"></i>
									  </span>
									</div>
									<input type="text" name='nome' id='nome' class="form-control" placeholder="Nome">
								  </div>


								    <div class="input-group no-border">	
										<div class="input-group-prepend">
										<span class="input-group-text">
											<i class="now-ui-icons business_badge"></i>
									    </span>	
										</div>	
										<select class="form-control" name='id_pessoas_tipos' id='id_pessoas_tipos'>
											<option  style='background:#f96332' selected='true' disabled='disabled' value=''>Selecione uma opção</option>
											<?php
											$objPessoasTipos = $gestor->retornarPessoasTipos('','','pessoas_tipos','tipo','ASC');
											if ($objPessoasTipos){
												foreach($objPessoasTipos as $oo){
													if($oo->id > 1){
											?>
												<option  style='background:#f96332' value='<?=$oo->id?>'><?=$oo->tipo?></option>
											<?php
													}	
												}

											}
											?>
										</select>
									</div>

									<div class="input-group no-border">
									<div class="input-group-prepend">
									  <span class="input-group-text">
										<i class="now-ui-icons users_circle-08"></i>
									  </span>
									</div>
									<input type="text" name='responsavel' id='responsavel' class="form-control" placeholder="Responsável">
								  </div>
								  
								  <div class="input-group no-border">
									<div class="input-group-prepend">
									  <span class="input-group-text">
										<i class="now-ui-icons ui-1_email-85"></i>
									  </span>
									</div>
									<input type="text" name='email' id='email' class="form-control" placeholder="Email">
								  </div>
								  <div class="input-group no-border">
									<div class="input-group-prepend">
									  <span class="input-group-text">
										<i class="fas fa-key"></i>
									  </span>
									</div>
									<input type="password" name='senha' id='senha' class="form-control" placeholder="Senha">
								  </div>
								  <div class="input-group no-border">
									<div class="input-group-prepend">
									  <span class="input-group-text">
										<i class="fas fa-key"></i>
									  </span>
									</div>
									<input type="password" class="form-control" name='confirma_senha' id='confirma_senha' placeholder="Confirme sua senha">
								  </div>
								  <div class="input-group no-border">
									<div class="input-group-prepend">
									  <span class="input-group-text">
										<i class="fas fa-phone"></i>
									  </span>
									</div>
									<input type="text" class="form-control" name='telefone' id='telefone' placeholder="Telefone">
								  </div>

								  <div class="input-group no-border">
									<div class="input-group-prepend">
									  <span class="input-group-text">
										<i class="fas fa-phone"></i>
									  </span>
									</div>
									<input type="text" class="form-control" name='cep' id='cep' placeholder="CEP">
								  </div>

								  <div class="input-group no-border">
									<div class="input-group-prepend">
									  <span class="input-group-text">
										<i class="fas fa-phone"></i>
									  </span>
									</div>
									<input type="text" class="form-control" name='bairro' id='bairro' placeholder="Bairro">
								  </div>


								  <div class="input-group no-border">
									<div class="input-group-prepend">
									  <span class="input-group-text">
										<i class="fas fa-phone"></i>
									  </span>
									</div>
									<input type="text" class="form-control" name='endereco' id='endereco' placeholder="Endereço">
								  </div>

								  <div class="input-group no-border">
									<div class="input-group-prepend">
									  <span class="input-group-text">
										<i class="fas fa-phone"></i>
									  </span>
									</div>
									<input type="text" class="form-control" name='numero' id='numero' placeholder="Número">
								  </div>


								  <div class="input-group no-border">
									<div class="input-group-prepend">
									  <span class="input-group-text">
										<i class="fas fa-phone"></i>
									  </span>
									</div>
									<input type="text" class="form-control" name='cidade' id='cidade' placeholder="Cidade">
								  </div>


								  <div class="input-group no-border">
									<div class="input-group-prepend">
									  <span class="input-group-text">
										<i class="fas fa-phone"></i>
									  </span>
									</div>
									<input type="text" class="form-control" name='uf' id='uf' placeholder="Estado">
								  </div>
								  
								  
								  
								  
								
								<div class="text-center">
								  <button type='button' id='btnSalvarCuidador' name='btnSalvarCuidador' onclick='EnviarForm()' class="btn  btn-primary btn-simple btn-round  btn-lg">Cadastrar</button>
								</div>
															 
								 <div class="social-line text-center">
									  <br>
									<a style='' href="../../splash" class="">Voltar para o início</a>.
									
								  </div>
								 
								 
							
						</div>					  
						
		  
		  
		  
		</form>   
		
			
	</div>
 
 
 
  <!--   Core JS Files   -->
  <?php
 require("../../inc/scripts2.php") ;
  ?>
  <script type="text/javascript">
  
function EnviarForm(){
	var err = '';

	var quantidadeErros = 0;

	var mensagem = '';

	var tipoMensagem = 'cadastrado';
	var tipoMensagem2 = 'cadastrar';
	var dialog;
	
	if ($('#nome').val() == ''){
		quantidadeErros++;
		mensagem += '- Nome (obrigatório)<br />'
	}
	if ($('#email').val() == ''){
		quantidadeErros++;
		mensagem += '- Email (obrigatório)<br />'
	}
	if ($('#senha').val() == ''){
		quantidadeErros++;
		mensagem += '- Senha (obrigatório)<br />'
	}
	if ($('#confirma_senha').val() == ''){
		quantidadeErros++;
		mensagem += '- Confirmação de senha (obrigatório)<br />'
		
	} else {
		if ($('#confirma_senha').val() != $('#senha').val()){
			quantidadeErros++;
			mensagem += '- Confirmação de senha e senha não coincidem<br />'
		}
	}
	
	if (quantidadeErros>0){

			if (quantidadeErros>1){

				err = 'Foram encontrados os seguintes erros:';
			
			} else {

				err = 'Foi encontrado o seguinte erro:';

				
			}
			
			bootbox.hideAll()

			var dialog = bootbox.dialog({ 
				title: err, 
				message: mensagem,
				buttons: {
					
					ok: {
					  label: "OK",
					  className: "",
					  callback: function() {
						dialog.modal('hide');
					  }
					}
				}
			});
			
			

	 } else {
	 


		$.post('process.php',$('#formPrincipal').serialize(),
		
			function(data){					   
								
				if(data == 1){ // 1 ou maior 
								
						tituloMensagem = 'Sucesso:';						
									
						mensagem = 'Seu cadastro realizado com sucesso.';													
							

						var dialog = bootbox.dialog({ 
							title: tituloMensagem, 
							message: mensagem,
							buttons: {
								
								inserir: {
								  label: "Ok",
								  className: "",
								  callback: function() {											
										document.location.href = "../../home";	
									
								  }
								
								}
							}		  
								
						});
					
									
						
								
				} else { //erro
						if(data == 2){		
							tituloMensagem = 'Erro:';
										
							tipo = 'erro';
										
							mensagem = 'Não foi possível realizar o cadastro, email utilizado anteriormente.';
						} else {
							tituloMensagem = 'Erro:';
										
							tipo = 'erro';
										
							mensagem = 'Não foi possível realizar o cadastro.';

						}
							
						bootbox.hideAll()
						var dialog = bootbox.dialog({ 
							title: tituloMensagem, 
							message: mensagem,
							buttons: {
								
								ok: {
								  label: "Ok",
								  className: "btn-danger",
								  callback: function() {
									dialog.modal('hide');
								  }
								}
							}			  
								
						});
				}

		});					 		
	 }	
	
}
  
 	      
 jQuery(document).ready(function(){
	
	$("#telefone").mask("(99) 99999999?9"); 
	
	
		
	
	
});	 
  </script>  
</body>

</html>
