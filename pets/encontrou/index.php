<?php 
/**
 * 
 * 
*	@autor Flávio Silva Brandão <flavio@afixo.com.br>
*	@version 15-05-2021 as 09:00:30
*
* 
* */



/*
 * habilitando o uso de sessoes
 * */

ob_start();
session_start();

/*
 * verificando se o usuario esta logado senao redireciona
 * */
if (!$_SESSION['ENCONTRAPET_codigo']){
	header("Location: ../../splash");
}



# 
# REQUIRES OBRIGATORIOS
#


require('../../classes/gestor.php');

#
# INSTANCIA DE OBJETOS OBRIGATORIAS
#


$util = new Util();
$gestor   = new Gestor();    

#
# TRATAMENTO DE INJECTION
#

$_POST = $util->validaParametro($_POST);
$_GET  = $util->validaParametro($_GET);
?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="../../assets/img/apple-icon.png">
  <link rel="icon" type="image/png" href="../../assets/img/favicon.png">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    EncontraPet - Início
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <?php
  require("../../inc/css2.php");
  ?>
</head>

<body class="text-center" style=''>


              
       
              

            <div class="" style='padding:0'>      
				
				<?php
				require("../../inc/navegacao2.php")
				?>
            

              <!-- Nav tabs -->
              <div style="">
				  
				       
				  

                <div class="" style='padding-left:3px; padding-right:3px; padding-top:5px; '>
                  <!-- Tab panes -->
                  <div class="tab-content text-center">

					  
                    <div class="tab-pane active" id="doar" role="tabpanel">
<form class="form" method="" action="" id='formPrincipal' name='formPrincipal'>
	<input type='hidden' name='operacao' id='operacao' value='inserir'>
							<div class='container-fluid text-left'>
								
							  
							  
											<div class="row">
												<div class="col-12">
													
													<h5 class="" style=''>Informe os dados do pet encontrado</h5>               
												</div>    
											</div>				                  
											<div class="row">
																		
												<div class="col-12 col-sm-6">							
													  
														<label for='id_especies' style='display:block; '>Espécie?</label>
														
														<select  class="form-control" name='id_especies' id='id_especies'>					
															
															<option value=''>Selecione</option>
															<option value='1'>Cães</option>
															<option value='2'>Gatxs</option>
														</select>
												
												</div>							
		
												<div class="col-12 col-sm-6">							
													  
														<label for='id_racas' style='display:block; '>Raça?</label>
														
														<select  class="form-control" name='id_racas' id='id_racas'>					
															
															<option value='0'>Não sei</option>

														</select>
												
												</div>							
					
												<div class="col-12 col-sm-6">															  
													  
															<label for='cep' style='display:block; '>Informe CEP de onde o pet se encontra:</label>
															<input type="text" name='cep' id='cep' class="form-control" placeholder="CEP" maxlength='9'>
												</div>		  
												<input type='hidden' id='bairro' name='bairro' value=''>
												<input type='hidden' id='cidade' name='cidade' value=''>
												<input type='hidden' id='estado' name='estado' value=''>
												<input type='hidden' id='rua' name='rua' value=''>
													 		
												<div class="col-12 col-sm-6" id='divRespostaCep'>															  
													  
													
													 							
												</div>
											</div>	
											<div class="row" style='padding-top:10px'>			
												<div class="col-12">															  
													  
															<label for='arquivo' style='display:block; '>Imagem:</label>
																							
																					
														
															<input id="arquivo"  class='arquivo' type="file"  accept="image/*" name="arquivo">
																					
															<input type='hidden' id='imagem' name='imagem' value=''>
													 							
												</div>
											</div>	
											
											<div class="row" style='padding-top:10px'>		
												<div class="col-12">															  
													  
															<label for='descricao' style='display:block; '>Descreva o animal:</label>
														
																								  
													  
														<textarea  class="form-control" name='descricao' id='descricao' style='display:block; '></textarea>
													  
												</div>	
											</div>	
											<div class="row" style='padding-top:10px'>		
												
												<div class="col-12 col-sm-4">															  
													  
													  <label for='sexo' style='display:block; '>Sexo?</label>
																					  
												  <select  class="form-control" name='sexo' id='sexo' style='display:block; '>					
													  
													  <option value='Não sei'>Não sei</option>
													  <option value='Macho'>Macho</option>
													  <option value='Fêmea'>Fêmea</option>
													  
												  </select>
																		   
										 	 	</div>	  
												
												<div class="col-12 col-sm-4">															  
													  
															<label for='idade' style='display:block; '>Idade?</label>
														
																									  
													  
														<select  class="form-control" name='idade' id='idade' style='display:block; '>					
															<option value='Não sei'>Não sei</option>
															<option value='1 a 6 meses'>1 a 6 meses</option>
															<option value='6 meses a 1 ano'>6 meses a 1 ano</option>
															<option value='1 a 2 anos'>1 a 2 anos</option>
															<option value='2 a 3 anos'>2 a 3 anos</option>
															<option value='3 a 4 anos'>3 a 4 anos</option>
															<option value='5 ou mais anos'>5 ou mais anos</option>
															
														</select>
													  
												</div>		  
											</div>	
											<div class="row" style='padding-top:10px'>		
												<div class="col-12">	  
																					  
													  
													  
													  
													
													<div class="text-center">
													  
													  <button type='button' id='btnDoar' name='btnDoar' onclick='EnviarForm()' class="btn  btn-primary btn-round btn-lg">Cadastrar</button>
													  
													</div>
																				 
													 
													 
													 
												
												</div>					  
											</div>					  
											
							  
							  
							  
							
						</div>				
						</form> 
					</div>
                 
                 
                    
                  </div>
                  
						               
						  
                </div>




				<?php
                require("../../inc/rodape2.php");
                ?>
 



                
              </div>
			
			
            
            
            </div>
         
       
                       
              
                     
              
              
           </div>
         

 
  <!--   Core JS Files   -->
  <?php
 require("../../inc/scripts2.php") ;
  ?>
  
  <script>
function EnviarForm(){
	
	var err = '';

	var quantidadeErros = 0;

	var mensagem = '';

	var tipoMensagem = '';


	tipoMensagem = 'inserido';

	tipoMensagem2 = 'inserir';
	
	if ($('#id_especies').val() == ''){
		quantidadeErros++;
		mensagem += '- Espécie (obrigatório)<br />'
	}	
	if ($('#cep').val() == ''){
		quantidadeErros++;
		mensagem += '- CEP onde o pet se encontra (obrigatório)<br />'
	}
	
	if ($('#imagem').val() == ''){
		quantidadeErros++;
		mensagem += '- Imagem (obrigatório)<br />'
	}



	if (quantidadeErros>0){

		if (quantidadeErros>1){

			err = 'Foram encontrados os seguintes erros:';
		
		} else {

			err = 'Foi encontrado o seguinte erro:';

			
		}

		
		//bootbox.hideAll();

		var dialog = bootbox.dialog({ 
			title: err, 
			message: mensagem,
			buttons: {
				
				ok: {
				  label: "OK",
				  className: "btn-primary",
				  callback: function() {
					dialog.modal('hide');
				  }
				}
			}
		});	
	
			
		

	 } else {
		

		$.post('process.php',$('#formPrincipal').serialize(),
		
		function(data){					   
							
			if(data > 0){ // 1 ou maior 
							
				tituloMensagem = 'Sucesso:';
								
				tipo = 'sucesso';
							
				mensagem = 'Pet colocado para ser Encontrado com sucesso.';													
					
		
				var dialog = bootbox.dialog({ 
					title: tituloMensagem, 
					message: mensagem,
					buttons: {
						
						inserir: {
						  label: "Cadastrar outro",
						  className: "btn-primary",
						  callback: function() {
							document.location.href = '../../pets/encontrou';
						  }
						},
						listar: {
						  label: "OK",
						  className: "btn-primary",
						  callback: function() {
							document.location.href = '../../pets';
						  }
						}
					},					
					onEscape: function() {
						document.location.href = '../../pets/encontrou';
					}
				});
				
						
					
					
							
			} else { //erro
							
				tituloMensagem = 'Erro:';
							
				tipo = 'erro';
							
				mensagem = 'Não foi possível cadastrar o pet.';
							
					
					bootbox.hideAll()
					var dialog = bootbox.dialog({ 
						title: tituloMensagem, 
						message: mensagem,
						buttons: {
							
							ok: {
							  label: "Ok",
							  className: "btn-primary",
							  callback: function() {
								dialog.modal('hide');
							  }
							}
						},					
						onEscape: function() {
							dialog.modal('hide');
						}		  
							
					});
			}
							
			
							
		});					 
	}
}		  
		  
	  
function buscaEndereco(cep){
		$.get( "https://brasilapi.com.br/api/cep/v1/"+cep, function( data ) {
			//$( ".result" ).html( data );
			//alert( data.state +  " - "  + data.city + " - " + data.neighborhood );
		
			$('#rua').val(data.street);
			$('#bairro').val(data.neighborhood);
			$('#cidade').val(data.city);
			$('#estado').val(data.state);
		
			$('#divRespostaCep').html("<div style='padding-top:40px'>" + data.neighborhood +  ", "  + data.city + " - " + data.state + "</div>");			
		}, "json" )  .fail(function() {
					$('#rua').val('');
					$('#bairro').val('');
					$('#cidade').val('');
					$('#estado').val('');
					$('#divRespostaCep').html("");
					bootbox.hideAll()
					var dialog = bootbox.dialog({ 
						title: "Erro", 
						message: "Não foi possível localizar este CEP, por favor utilize outro.",
						buttons: {
							
							ok: {
							  label: "Ok",
							  className: "btn-primary",
							  callback: function() {
								dialog.modal('hide');
							  }
							}
						},					
						onEscape: function() {
							dialog.modal('hide');
						}		  
							
					});
		});
					  			  
		
	}	
	$( "#cep" ).change(function() {
		
		//if ($('#cep').val().length >= 8){
			buscaEndereco($('#cep').val())
		//	alert($('#cep').val()) 
		//} else {

			
		//}
	});
	
	function mudaRaca(especie){
				  
			   var especie = $('#id_especies :selected').val();
			   
			  $('#divRacas').load('racas.php?especie='+especie);
			
	}		
	$('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
	  var url = $(this).attr("href"); // the remote url for content
	  var target = $(this).data("target"); // the target pane
	  var tab = $(this); // this tab
	  
	  // ajax load from data-url
	  $(target).load(url,function(result){      
		tab.tab('show');
	  });
	});

	// initially activate the first tab..
	//$('#tabIni').click();  
	
	//$('').load(url,function(result){      
	//	tab.tab('show');
	 // });
	 
	function enviaArquivo(){

		$.ajax( {
		  url: 'process.php?operacao=inserir_arquivo',
		  type: 'POST',
		  data: new FormData($("#formPrincipal")[0]),
		  processData: false,
		  success: function( data, textStatus ){		  

				if (data != "0"){ // sucesso					
				   
					$( "#imgOld" ).remove();
					$( "#imagem" ).after( "<img src='../../uploads/tmp/"+data+"' id='imgOld' width='300px' style='margin:0 auto; display:block' />" );
				
					
							
					$( "#imagem" ).val(data);
				} else {

					bootbox.hideAll()
					var dialog = bootbox.dialog({ 
						title: "Erro", 
						message: "Não foi possível carregar a imagem, por favor utilize outra.",
						buttons: {
							
							ok: {
							  label: "Ok",
							  className: "btn-primary",
							  callback: function() {
								dialog.modal('hide');
							  }
							}
						},					
						onEscape: function() {
							dialog.modal('hide');
						}		  
							
					});
					
				}
			  
		  },
		  contentType: false
		});
		
	}	 
	  
	 $(document).ready(function() {
		$("#cep").mask("99999-999"); 
			var files;
			// Add events
			$('input[type=file]').on('change', enviaArquivo);  
	  
	   });
  </script>  
</body>

</html>
