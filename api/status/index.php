<?php 
/**
*	@autor Flávio Silva Brandão <flavio@afixo.com.br>
*	@version 24-11-2020 as 09:04:28
*/

require('../includes/sessions.php');
require('../../classes/gestor.php');

$util = new Util();
$gestor   = new Gestor();

extract($_GET);
$removerColuna = array('id');
$resultado = $gestor->retornarStatus('');

if ($resultado){
	if (count($resultado) < 2){
		$resultado = array($resultado);
	}
}

if ($_GET['operacao'] == 'editar' or $_GET['operacao'] == 'visualizar'){
	$id = $_GET['id'];
	$objeto = $gestor->retornarStatus($id);
	$operacao = $_GET['operacao'];
} else {
	$operacao = 'inserir';	
}

$subSessao = ' ' . ucfirst($operacao);

if ($objeto){
	
#
# VARIAVEIS 
#
$id = $objeto->id;
$status = $objeto->status;

}

$operacao = $_GET['operacao'];

if (empty($operacao)){
	$operacao = $_POST['operacao'];
}

extract($_POST);
extract($_GET);

# 
# VARIAVEIS  
# 

$objeto = array(array(id=>$id,status=>$status));

switch ($operacao) {
	case 'inserir':	
		$obj = $gestor->inserirStatus($objeto);		
		if ($obj){
				die('1');
		} else {
				die('0');
		}	

	break;
	
	case 'editar':
		$obj = $gestor->editarStatus($objeto);		
		if ($obj){
				die('1');
		} else {
			die('0');
		}	

	break;
	
	case 'excluir':
		$id  = $_GET['id'];
		$obj = $gestor->excluirStatus($id);
		if ($obj){
				die('1');
		} else {
			die('0');
		}	
	break;

}
?>
<?php 
header("content-type:application/json");
/**
*	@autor Flávio Silva Brandão <flavio@afixo.com.br>
*	@version 24-11-2020 as 09:04:28
*/

require('../../classes/gestor.php');

$util 	= new Util();
$gestor = new Gestor();

/* verificacao para direcionamento de get ou post */
if (!$_POST){
	
	extract($_GET);

	if (!@$id){ //  se não foi informado a quem buscar, buscar todos

		$objeto = $gestor->retornarTipos('');

		if ($objeto){
			if (count($objeto) < 2){
				$objeto = array($objeto);
			}
		}

	} else { // buscar a quem foi informado

		$objeto = $gestor->retornarTipos($id);
	}

	//print var_dump($objeto);
	print json_encode($objeto);

} else {

	extract($_POST);	

	switch (@$operacao) {
		case 'inserir':	

			@$objeto = array(array('tipo'=>$tipo));
			$obj = $gestor->inserirTipos($objeto);		
			if ($obj){
				print($obj);
			} else {
				print(false);
			}	

		break;
		
		case 'editar':

			@$objeto = array(array('id'=>$id,'tipo'=>$tipo));
			$obj = $gestor->editarTipos($objeto);		
			if ($obj){
				print(true);
			} else {
				print(false);
			}	

		break;
		
		case 'excluir':
			
			$obj = $gestor->excluirTipos($id);
			if ($obj){
				print(true);
			} else {
				print(false);
			}	
		break;

	}
}
?>