<?php
/**
*	@autor Flávio Silva Brandão <flavio@afixo.com.br>
*	@version 12-05-2021 as 07:34:17
*/

class Racas extends DB {

	private $tabela = 'racas';
	private $id;
	private $raca;
	private $id_especies;

	/**
	* Método construtor do objeto especies  
	 @param inteiro id
	 @param string raca
	 @param inteiro id_especies

	**/
	public function __construct($id="",$raca="",$id_especies="") {
		$this->id = $id;
		$this->raca = $raca;
		$this->id_especies = $id_especies;
	}

	public function retornar($id, $campo = 'id', $tabela = 'racas', $ordenadoPor = '', $order = '', $limit = '') {
		return parent::retornar($id, $campo, $tabela, $ordenadoPor, $order, $limit);
	}

	public function inserir($objeto, $tabela = 'racas') {
		return parent::inserir($objeto, $tabela);
	}

	public function editar($objeto, $campo = 'id', $tabela = 'racas') {
		return parent::editar($objeto, $campo, $tabela);
	}

	public function excluir($valor, $campo = 'id', $tabela = 'racas'){
		return parent::excluir($valor, $campo, $tabela);
	}

}
?>
