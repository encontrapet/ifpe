<?php 
ob_start();
session_start();
/**
*
*	@autor Flávio Silva Brandão <flavio@afixo.com.br>
*	@version 29-05-2021 as 12:06:55
*/



# 
# REQUIRES OBRIGATORIOS
#

require('../../classes/gestor.php');
require('../../classes/util/Zebra_Image.php');
$util = new Util();
$gestor   = new Gestor();
$image = new Zebra_Image();

#
# TRATAMENTO DE INJECTION
#

$_POST = $util->validaParametro($_POST);


extract($_POST);
extract($_GET);

# 
# VARIAVEIS  
# 

$diretorio = "../../uploads/pet/";
$diretorioTmp = "../../uploads/tmp/";

$arquivo = @$_FILES['arquivo'];


//var_dump($_REQUEST);
//die;
switch ($operacao) {
	
	case 'editar':			

			$data = date('Y-m-d');
			$hora = date('H:i:s');
			//die(var_dump($_REQUEST));
			if(@$imagem){ // se existe imagem

				$objeto = array(array('id'=>$pet,'id_especies'=>$id_especies, 'id_racas'=>$id_racas , 'descricao'=>$descricao, 'sexo'=>$sexo,'castrado'=>$castrado, 'idade'=>$idade,'vacinado'=>$vacinado, 'vermifugado'=>$vermifugado, 'cep'=>$cep,'estado'=>@$estado,'cidade'=>@$cidade,'bairro'=>@$bairro,'rua'=>@$rua,'pedigree'=>$pedigree, 'data_cadastro'=>$data . ' '. $hora));	
				//die(var_dump($objeto));			
				
				$imagem = trim($imagem);							
				$arrArq = explode('.',$imagem);
				
				rename($diretorioTmp.$imagem,$diretorio.$imagem);		
				
				$caminho = $diretorio . $imagem; // upload caminho do arquivo enviado	

				// indicate a source image
				$image->source_path = $caminho;
				
				
				$ext = substr($image->source_path, strrpos($image->source_path, '.') + 1);

				// indicate a target image
				$image->target_path = $diretorio.$arrArq[0].'M.' . $ext;
						
				//die($image->target_path . "<<");
				
				// resize
				// and if there is an error, show the error message
				if (!$image->resize(800, 800, ZEBRA_IMAGE_NOT_BOXED, -1)) { 					
					//show_error($image->error, $image->source_path, $image->target_path);														
					
				} 	else {

					@chmod ( $image->target_path, 0777);
				}

				$obj = $gestor->editarPets($objeto); // insersao do arquivo principal
				if ($obj){	
					/*
					 * inserir imagem e movimentacao */																							
					
					$objeto2 = array(array('id_pets'=>$pet, 'caminho'=>$imagem, 'data'=>$data));				
					$gestor->inserirFotos($objeto2); // insersao do tamanho											
					
					die('1');
				} else { // erro ao inserir o arquivo padrão			
					die('0');
				}
				
			} else { // erro ao inserir o arquivo padrão
				$objeto = array(array('id'=>$pet,'id_especies'=>$id_especies, 'id_racas'=>$id_racas , 'descricao'=>$descricao, 'sexo'=>$sexo,'castrado'=>$castrado, 'idade'=>$idade, 'vacinado'=>$vacinado, 'vermifugado'=>$vermifugado, 'cep'=>$cep,'estado'=>@$estado,'cidade'=>@$cidade,'bairro'=>@$bairro,'rua'=>@$rua,'pedigree'=>$pedigree, 'data_cadastro'=>$data . ' '. $hora));	
				$obj = $gestor->editarPets($objeto);
				if ($obj){	
					die('1');
				} else { // erro ao inserir o arquivo padrão			
					die('0');
				}
			}			
				

	break;

	case 'inserir_arquivo': /*  */

		$upload = $util->uploadGenerics($arquivo, $diretorioTmp);

		if(is_array($upload)){ // ocorreu erro no envio retorno array de erros
			$erro = $upload; 	
			
			die('0');	
		} else {
			$caminho = $upload; // upload caminho do arquivo enviado											
			die($caminho);	
		}	
	
	break;	
	case 'excluir': /*  */

		$objeto = array(array('id'=>$pet, 'id_status'=>5));	
		$obj = $gestor->editarPets($objeto);
		if ($obj){	
			die('1');
		} else { // erro ao inserir o arquivo padrão			
			die('0');
		}
	
	break;	
}	
?>
