<!--

=========================================================
* Now UI Kit - v1.3.0
=========================================================

* Product Page: https://www.creative-tim.com/product/now-ui-kit
* Copyright 2019 Creative Tim (http://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/now-ui-kit/blob/master/LICENSE.md)

* Designed by www.invisionapp.com Coded by www.creative-tim.com

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

-->
<!DOCTYPE html>
<html lang="pt-br">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="../../assets/img/apple-icon.png">
  <link rel="icon" type="image/png" href="../../assets/img/favicon.png">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    EncontraPet - Início
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
	<?php
	require("../../inc/css2.php");
	?>
  
</head>

<body style="padding-top:20px">

			<div class="col-12 col-lg-4 offset-lg-4 text-center" data-background-color="orange" style='padding-top:30px; padding-bottom:30px; '>
			<form class="form" method="" action="" id='formPrincipal' name='formPrincipal'>
                <div class="">    
                <img src='../../assets/img/logo.png' width='200px' />
                <h2 class="">EncontraPet</h2>
				        </div>
                
                  
                  <div class="input-group no-border">
                    <div class="input-group-prepend">
                      <span class="input-group-text">
                        <i class="now-ui-icons ui-1_email-85"></i>
                      </span>
                    </div>
                    <input type="text" class="form-control" placeholder="Email..." name='email' id='email'>
                  </div>
                  
                  <div class="social-line">
					  <a id="btnRecuperar" href="javascript:void(0)" class="btn  btn-primary btn-simple btn-round  btn-lg" onclick='EnviarForm()'>Recuperar</a>
                    
                  </div> 
                  
                  <div class="social-line">
					  <br>
                    <a style='' href="../../splash" class="">Voltar para o início</a>.
                    
                  </div>
                                  
				               
              </form>
            </div>
         

 
  <!--   Core JS Files   -->
  <?php
 require("../../inc/scripts2.php") ;
  ?>
  <script type="text/javascript">
  
function EnviarForm(){
	var err = '';

	var quantidadeErros = 0;

	var mensagem = '';

	var tipoMensagem = 'cadastrado';
	var tipoMensagem2 = 'cadastrar';
	var dialog;
	
	
	if ($('#email').val() == ''){
		quantidadeErros++;
		mensagem += '- Email (obrigatório)<br />'
	}
	
	if (quantidadeErros>0){

			if (quantidadeErros>1){

				err = 'Foram encontrados os seguintes erros:';
			
			} else {

				err = 'Foi encontrado o seguinte erro:';

				
			}
			
			bootbox.hideAll()

			var dialog = bootbox.dialog({ 
				title: err, 
				message: mensagem,
				buttons: {
					
					ok: {
					  label: "OK",
					  className: "",
					  callback: function() {
						dialog.modal('hide');
					  }
					}
				}
			});
			
			

	 } else {
	 


		$.post('process.php?opc=recuperar',$('#formPrincipal').serialize(),
		
			function(data){					   
								
				if(data == 1){ // 1 ou maior 
								
						tituloMensagem = 'Sucesso:';						
									
						mensagem = 'Conseguimos encontrar seu cadastro, confira seu email e siga as instruções.';													
							

						var dialog = bootbox.dialog({ 
							title: tituloMensagem, 
							message: mensagem,
							buttons: {
								
								inserir: {
								  label: "Ok",
								  className: "",
								  callback: function() {											
										document.location.href = "../../home";	
									
								  }
								
								}
							}		  
								
						});
					
									
						
								
				} else { //erro
						
							tituloMensagem = 'Erro:';
										
							tipo = 'erro';
										
							mensagem = 'Não foi possível recuperar o sua senha.';

						
							
						bootbox.hideAll()
						var dialog = bootbox.dialog({ 
							title: tituloMensagem, 
							message: mensagem,
							buttons: {
								
								ok: {
								  label: "Ok",
								  className: "btn-danger",
								  callback: function() {
									dialog.modal('hide');
								  }
								}
							}			  
								
						});
				}

		});					 		
	 }	
	
}
</script>
</body>

</html>
