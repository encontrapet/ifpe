<?php 
ob_start();
session_start();

/**
*	@autor Flávio Silva Brandão <flavio@afixo.com.br>
*	@version 21-01-2020 as 09:00:30
*/

/*
 * verificando se o usuario esta logado senao redireciona
 * */
if (!$_SESSION['ENCONTRAPET_codigo']){
	header("Location: ../../splash");
}

# 
# REQUIRES OBRIGATORIOS
#


require('../../classes/gestor.php');

#
# INSTANCIA DE OBJETOS OBRIGATORIAS
#


$util = new Util();
$gestor   = new Gestor();    

#
# TRATAMENTO DE INJECTION
#

$_POST = $util->validaParametro($_POST);
$_GET  = $util->validaParametro($_GET);

extract($_GET);
/*
 * 
 * reuperar os pets cadastrados anteriormente */
if (!$pet){
	header("location:../");
}
$objetoPets = $gestor->retornarPets($pet);

$objetoInteracoesPets = $gestor->retornarInteracoesPets(array($pet,$_SESSION['ENCONTRAPET_codigo']),array('id_pets','id_pessoas'));

//var_dump($objetoInteracoesPets);
//die;
if ($objetoInteracoesPets){

	header("location:../interesse/?pet=$pet");

}
if ($objetoPets){
	if (@count($objetoPets) < 2){
		$objetoPets = array($objetoPets);
	}
} else {

	header("location:./");
}

//var_dump($objetoPets);
//die;
?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="../../assets/img/apple-icon.png">
  <link rel="icon" type="image/png" href="../../assets/img/favicon.png">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    EncontraPet - Início
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <?php
  require("../../inc/css2.php");
  ?>
  
<style type="text/css">

</style>
</head>

<body class="text-center" style=''>


              
       
              

            <div class="" style='padding:0'>      
				
				<?php
				require("../../inc/navegacao2.php")
				?>
            

              <!-- Nav tabs -->
              <div style="">
				  
				       
				  

                <div class="" style='padding-left:3px; padding-right:3px; padding-top:5px; '>
                  <!-- Tab panes -->
                  <div class="tab-content text-center">

					  
                    <div class="tab-pane active" id="pets" role="tabpanel">
						<div class='container'>
						<?php
						foreach($objetoPets as $obj){
							
							
							$imagem = $gestor->retornarPets($obj->id,'id_pets','fotos','id','DESC',1)->caminho;
							$dataHora = $gestor->retornarMovimentacoesPets(array($obj->id,1),array('id_pets','id_status'),'movimentacoes_pets','id','DESC',1)->data_hora;
							
							if (!$imagem){
								if ($obj->id_especies == 1){ // cachorros
									$imagem = '../../assets/img/pets/black-dog.png'; 
								} else {
									$imagem = '../../assets/img/pets/black-cat.png'; 
								}
								
							} else {
								
								$imagem = '../../uploads/pet/' . $imagem;
							}
								
							if ($dataHora){
								$dataHora= $util->retornaData($dataHora,'br');
							}
						?>
						
							<div class='row'>
								<div class='col-sm-8'>
									
									
								<img src='<?=$imagem?>' style='width:100%'/>
								<div style='text-align:right; font-weight:bold; display:block; clear:right; margin-top:5px'><small>Pet cadastrado em <?php								
								
									echo $dataHora;
																
								?></small></div>
								<p style='text-align:left'>

								<?php					
														if ($obj->cidade && $obj->estado ) {echo " " . $obj->cidade . " - " . $obj->estado . ", "; }			
														if ($obj->idade != "Não sei"){
															
																echo " " . $obj->idade . ', ';
																
															
														}								
														?>
														<?php								
														if ($obj->id_racas > 0){
															echo '#raca';
														}								
														?>
														<?php								
														if ($obj->sexo != "Não sei"){
															echo '#'. $obj->sexo;
														}								
														?>
														<?php								
														if ($obj->pedigree == "Sim"){
															echo '#pedigree';
														}								
														?>														
														<?php								
														if ($obj->castrado == "Sim"){
															if ($obj->sexo == "Fêmea"){											
																echo " #castrada";
															} else {
																echo " #castrado";
															}
														}								
														?>
														<?php								
														if ($obj->vacinado == "Sim"){
															if ($obj->sexo == "Fêmea"){										
																echo " #vacinada";
															} else {
																echo " #vacinado";
															}
															
															
														}								
														?>
														<?php								
														if ($obj->vermifugado == "Sim"){
															if ($obj->sexo == "Fêmea"){										
																echo " #vermifugada";
															} else {
																echo " #vermifugado";
															}
																
															
														}								
														?>							
								</p>
								<p  style='text-align:left'><?php								
								
									echo $obj->descricao;
																
								?></p>
								</div>
								<div class='col-sm-4 text-left' style='padding-top:0px; '>
								
								
								
									
								
								<?php
								$objInterassoes = $gestor->retornarInteracoesPets(array($_SESSION['ENCONTRAPET_codigo'],$obj->id), array('id_pessoas','id_pets'));		
								//var_dump($objInterassoes);
								if (!$objInterassoes){	
								?>
								<div style='padding-top:10px'>
								<hr />
								<h6>Tenho interesse</h6>
								</div>		
								
								<button class='btn btn-primary' style='width:100%' onclick='demonstrarInteresse("<?=$obj->id?>")' ><i class='fa fa-heart'></i> Tenho interesse</button>							
								<?php
								} else {
								?>


								<div style='display:block; clear:left; margin-top:0px'>
									<div style='padding:5px; background:#fff; width:35px; height:35px; text-align:center; float:left; padding-top:7px' class='rounded-circle'><i class='fa fa-heart' style='color:#e95e38' > </i>
									</div>
									<div style=' padding-top:5px'>
									<small style='padding-left:10px;' >Em <?=$util->retornaData($objInterassoes->data_hora,'br')?> você demonstrou interesse por esse pet.</small>
									</div>
								</div>

								<div style='padding-top:10px'>
								<hr />
								<h6>Enviar mensagem</h6>
								</div>								
								<?php
								}
								?>								
								
							</div>
						
						
						
						<?php
						}
						?>
						
						</div>
					</div>

                    
                    
                  </div>
                  
						               
						  
                </div>




				<?php
  require("../../inc/rodape2.php");
  ?> 



                
              </div>
			
			
            
            
            </div>
         
       
                       
              
                     
              
              
           </div>
         

 
		   <?php
  require("../../inc/scripts2.php");
  ?>
  <script>
	  
	  function demonstrarInteresse(quem){
		  
		  
		$.get('process.php?pet='+quem, $('#frmPrincipal').serialize(),
		
		function(data){					   
						//alert(data);	
			if(data > 0){ // 1 ou maior 
							
				tituloMensagem = 'Sucesso:';
								
				tipo = 'sucesso';
							
				mensagem = 'Ok, sabemos de seu interesse, envie uma mensagem e converse a respeito do pet.';													
					
				var dialog = bootbox.dialog({ 
					title: tituloMensagem, 
					message: mensagem,
					buttons: {
						
						inserir: {
						  label: "Ok",
						  className: "btn-primary",
						  callback: function() {
							document.location.href = '../../adotar/pet/?pet='+quem;
						  }
						}
					},					
					onEscape: function() {
						document.location.href = '../../adotar/pet/?pet='+quem;
					}
				});			
					
							
			} else { //erro
							
				tituloMensagem = 'Erro:';
							
				tipo = 'erro';
							
				mensagem = 'Não foi possível realizar a operação.';
							
					
					bootbox.hideAll()
					var dialog = bootbox.dialog({ 
						title: tituloMensagem, 
						message: mensagem,
						buttons: {
							
							ok: {
							  label: "Ok",
							  className: "btn-primary",
							  callback: function() {
								dialog.modal('hide');
							  }
							}
						},					
						onEscape: function() {
							dialog.modal('hide');
						}		  
							
					});
			}
							
			
							
		});				  
		  
	  }

	$('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
	  var url = $(this).attr("href"); // the remote url for content
	  var target = $(this).data("target"); // the target pane
	  var tab = $(this); // this tab
	  
	  // ajax load from data-url
	  $(target).load(url,function(result){      
		tab.tab('show');
	  });
	});

	// initially activate the first tab..
	$('#tabIni').click();  
  </script>  
</body>

</html>
