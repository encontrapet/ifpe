<?php 
/**
 * 
 * 
*	@autor Flávio Silva Brandão <flavio@afixo.com.br>
*	@version 26-05-2021 as 09:00:30
*
* 
* */

/*
 * habilitando o uso de sessoes
 * */

ob_start();
session_start();


# 
# REQUIRES OBRIGATORIOS
#


require('../../classes/gestor.php');

#
# INSTANCIA DE OBJETOS OBRIGATORIAS
#


$util = new Util();
$gestor   = new Gestor();    

#
# TRATAMENTO DE INJECTION
#

$_POST = $util->validaParametro($_POST);
$_GET  = $util->validaParametro($_GET);
/*
 * verificando se o usuario esta logado senao redireciona
 * */
if (!$_SESSION['ENCONTRAPET_codigo']){
	header("Location: ../../splash");
} else {

	$objUsuario = $gestor->retornarPessoas($_SESSION['ENCONTRAPET_codigo']);
	$nome  		= $objUsuario->nome;
	$email 	 	= $objUsuario->email;
	$telefone   = $objUsuario->telefone;
	$senha  	= $objUsuario->senha;
	$senha = $util->decripty($senha);
}




?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="../../assets/img/apple-icon.png">
  <link rel="icon" type="image/png" href="../../assets/img/favicon.png">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    EncontraPet - Início
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <?php
  require("../../inc/css2.php");
  ?>
</head>

<body class="text-center" style=''>


              
       
              

            <div class="" style='padding:0'>      
				
				<?php
				require("../../inc/navegacao2.php")
				?>
            

              <!-- Nav tabs -->
              <div style="">
				  
				       
				  

                <div class="" style='padding-left:3px; padding-right:3px; padding-top:5px; '>
                  <!-- Tab panes -->
                  <div class="tab-content text-center">

				 	<div class="tab-pane" id="novo" role="tabpanel">
					</div>
                    <div class="tab-pane active" id="geral" role="tabpanel">
<form class="form" method="" action="" id='formPrincipal' name='formPrincipal'>
	<input type='hidden' name='operacao' id='operacao' value='inserir'>
							<div class='container-fluid text-left'>
								
							  
							  
											<div class="row">
												<div class="col-12">
													
													<h5 class="" style=''>Meu cadastro</h5>               
												</div>    
											</div>				                  
												
											<div class="row" >						
												<div class="col-4" style='padding-top:20px'>															  
													  
													<label for='email' style='display:block; '>E-mail:</label>
													<input type="text" name='email' id='email' readonly class="form-control"  value="<?=$email?>" placeholder="Informe um nome">
												</div>		
												<div class="col-4" style='padding-top:20px'>															  
													  
													<label for='nome' style='display:block; '>Nome:</label>
													<input type="text" name='nome' id='nome' class="form-control" value="<?=$nome?>" placeholder="Informe um nome">
												</div>	
												<div class="col-4" style='padding-top:20px'>															  
													  
													<label for='telefone' style='display:block; '>Telefone:</label>
													<input type="text" name='telefone' id='telefone' class="form-control" value="<?=$telefone?>" placeholder="Informe um telefone">
												</div>	
												<div class="col-4" style='padding-top:20px'>															  
													  
													<label for='senha' style='display:block; '>Senha:</label>
													<input type="password" name='senha' id='senha' class="form-control" value="<?=$senha?>" placeholder="Informe uma senha">
												</div>
												<div class="col-4" style='padding-top:20px'>															  
													  
													<label for='confirma_senha' style='display:block; '>Confirmação:</label>
													<input type="password" name='confirma_senha' id='confirma_senha' class="form-control"  value="<?=$senha?>" placeholder="Informe uma confirmação">
												</div>

											</div>	
											
											<div class="row" style='padding-top:20px'>		
												<div class="col-12">	  
																					  
													  
													  
													  
													
													<div class="text-left">
													  
													  <button type='button' id='btnDoar' name='btnDoar' onclick='EnviarForm()' class="btn  btn-primary btn-round btn-lg">Atualizar</button>
													  
													</div>
																				 
													 
													 
													 <br>

													 <p>
													 <a href='javascript:void(0)' onclick='excluirCadastro()' style='color:#333'>Desejo excluir meu cadastro.</a>
													 </p>
												
												</div>					  
											</div>					  
											
							  
							  
							  
							
						</div>				
						</form> 
					</div>
                 
                    
                    <div class="tab-pane" id="adocao" role="tabpanel">
                      
                    </div>
                    <div class="tab-pane" id="perdidos" role="tabpanel">
                      
                    </div>                  
                    <div class="tab-pane" id="guia" role="tabpanel">				
                      
                    </div>
                    
                  </div>
                  
						               
						  
                </div>

				<?php
				require("../../inc/rodape2.php");
				?>


						   



                
              </div>
			
			
            
            
            </div>
         
       
                       
              
                     
              
              
           </div>
         

 
  <!--   Core JS Files   -->
  <?php
 require("../../inc/scripts2.php") ;
  ?>
  <script type="text/javascript">
  
function excluirCadastro(){

	bootbox.hideAll()
	
	var dialog = bootbox.dialog({ 
		title: 'Confirmação', 
		message: 'Deseja mesmo excluir seu cadastro?',
		buttons: {
			
			ok: {
				label: "Não",
				className: "",
				callback: function() {
				dialog.modal('hide');
				}
			},
			sim: {
				label: "Sim",
				className: "",
				callback: function() {
				
					$.post('process.php?operacao=excluir',$('#formPrincipal').serialize(),
		  
						function(data){					   
											
							if(data == 1){ // 1 ou maior 
											
									tituloMensagem = 'Sucesso:';						
												
									mensagem = 'Seu cadastro foi excluído com sucesso.';													
										

									var dialog = bootbox.dialog({ 
										title: tituloMensagem, 
										message: mensagem,
										buttons: {
											
											inserir: {
												label: "Ok",
												className: "",
												callback: function() {											
													document.location.href = "../../splash";	
												
												}
											
											}
										}		  
											
									});
								
												
									
											
							} else { //erro
									
									tituloMensagem = 'Erro:';
												
									tipo = 'erro';
												
									mensagem = 'Não foi possível excluir o cadastro.';

									
										
									bootbox.hideAll()
									var dialog = bootbox.dialog({ 
										title: tituloMensagem, 
										message: data,
										buttons: {
											
											ok: {
												label: "Ok",
												className: "btn-danger",
												callback: function() {
												dialog.modal('hide');
												}
											}
										}			  
											
									});
							}

					});				


				}
			}
		}
	});
}

  function EnviarForm(){
	  var err = '';
  
	  var quantidadeErros = 0;
  
	  var mensagem = '';
  
	  var tipoMensagem = 'cadastrado';
	  var tipoMensagem2 = 'cadastrar';
	  var dialog;
	  
	  if ($('#nome').val() == ''){
		  quantidadeErros++;
		  mensagem += '- Nome (obrigatório)<br />'
	  }
	  if ($('#email').val() == ''){
		  quantidadeErros++;
		  mensagem += '- Email (obrigatório)<br />'
	  }
	  if ($('#senha').val() == ''){
		  quantidadeErros++;
		  mensagem += '- Senha (obrigatório)<br />'
	  }
	  if ($('#confirma_senha').val() == ''){
		  quantidadeErros++;
		  mensagem += '- Confirmação de senha (obrigatório)<br />'
		  
	  } else {
		  if ($('#confirma_senha').val() != $('#senha').val()){
			  quantidadeErros++;
			  mensagem += '- Confirmação de senha e senha não coincidem<br />'
		  }
	  }
	  
	  if (quantidadeErros>0){
  
			  if (quantidadeErros>1){
  
				  err = 'Foram encontrados os seguintes erros:';
			  
			  } else {
  
				  err = 'Foi encontrado o seguinte erro:';
  
				  
			  }
			  
			  bootbox.hideAll()
  
			  var dialog = bootbox.dialog({ 
				  title: err, 
				  message: mensagem,
				  buttons: {
					  
					  ok: {
						label: "OK",
						className: "",
						callback: function() {
						  dialog.modal('hide');
						}
					  }
				  }
			  });
			  
			  
  
	   } else {
	   
  
  
		  $.post('process.php',$('#formPrincipal').serialize(),
		  
			  function(data){					   
								  
				  if(data == 1){ // 1 ou maior 
								  
						  tituloMensagem = 'Sucesso:';						
									  
						  mensagem = 'Seu cadastro foi atualizado com sucesso.';													
							  
  
						  var dialog = bootbox.dialog({ 
							  title: tituloMensagem, 
							  message: mensagem,
							  buttons: {
								  
								  inserir: {
									label: "Ok",
									className: "",
									callback: function() {											
										  document.location.href = "./";	
									  
									}
								  
								  }
							  }		  
								  
						  });
					  
									  
						  
								  
				  } else { //erro
						  
						tituloMensagem = 'Erro:';
									
						tipo = 'erro';
									
						mensagem = 'Não foi possível atualizar o cadastro.';

						  
							  
						  bootbox.hideAll()
						  var dialog = bootbox.dialog({ 
							  title: tituloMensagem, 
							  message: mensagem,
							  buttons: {
								  
								  ok: {
									label: "Ok",
									className: "btn-danger",
									callback: function() {
									  dialog.modal('hide');
									}
								  }
							  }			  
								  
						  });
				  }
  
		  });					 		
	   }	
	  
  }
	
			 
   jQuery(document).ready(function(){
	  
	  $("#telefone").mask("(99) 99999999?9"); 
	  
	  
		  
	  
	  
  });	 
	</script>  
</body>

</html>
