<?php 
/**
 * 
 * 
*	@autor Flávio Silva Brandão <flavio@afixo.com.br>
*	@version 29-05-2021 as 09:00:30
*
* 
* */



/*
 * habilitando o uso de sessoes
 * */

ob_start();
session_start();

/*
 * verificando se o usuario esta logado senao redireciona
 * */
if (!$_SESSION['ENCONTRAPET_codigo']){
	header("Location: ../../splash");
}



# 
# REQUIRES OBRIGATORIOS
#


require('../../classes/gestor.php');

#
# INSTANCIA DE OBJETOS OBRIGATORIAS
#


$util = new Util();
$gestor   = new Gestor();    

#
# TRATAMENTO DE INJECTION
#

$_POST = $util->validaParametro($_POST);
$_GET  = $util->validaParametro($_GET);

extract($_GET);

if (!$pet){
	header("Location: ../");
}
$objetoPets = $gestor->retornarPets(array($pet,$_SESSION['ENCONTRAPET_codigo']),array('id','id_pessoas'));
if (!$objetoPets){
	header("Location: ../");
}
$sexo = $objetoPets->sexo;
$castrado = $objetoPets->castrado;
$pedigree = $objetoPets->pedigree;
$vacinado = $objetoPets->vacinado;
$vermifugado = $objetoPets->vermifugado;

$id_especies = $objetoPets->id_especies;
$id_status = $objetoPets->id_status;
$cep = $objetoPets->cep;

$descricao = $objetoPets->descricao;
$imagem = $gestor->retornarFotos($objetoPets->id,'id_pets','fotos','id','DESC',1)->caminho;
$imagem = '../../uploads/pet/' . $imagem;							
?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="../../assets/img/apple-icon.png">
  <link rel="icon" type="image/png" href="../../assets/img/favicon.png">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    EncontraPet - Início
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <?php
  require("../../inc/css2.php");
  ?>
</head>

<body class="text-center" style=''>


              
       
              

            <div class="" style='padding:0'>      
				
				<?php
				require("../../inc/navegacao2.php")
				?>
            

              <!-- Nav tabs -->
              <div style="">
				  
				       
				  

                <div class="" style='padding-left:3px; padding-right:3px; padding-top:5px; '>
                  <!-- Tab panes -->
                  <div class="tab-content text-center">

					  
                    <div class="tab-pane active" id="doar" role="tabpanel">
<form class="form" method="" action="" id='formPrincipal' name='formPrincipal'>
	<input type='hidden' name='operacao' id='operacao' value='editar'>
	<input type='hidden' name='pet' id='pet' value='<?=$pet?>'>
							<div class='container-fluid text-left'>
								
							  
							  
											<div class="row">
												<div class="col-12">
													
													<h5 class="" style=''>Informe os dados do pet</h5>               
												</div>    
											</div>				                  
											<div class="row">
																		
												<div class="col-12 col-sm-6">							
													  
														<label for='id_especies' style='display:block; '>Espécie?</label>
														
														<select  class="form-control" name='id_especies' id='id_especies'>					
															
															<option value=''>Selecione</option>
															<option value='1' <?php if ($id_especies == "1") { echo "selected"; } ?>>Cães</option>
															<option value='2' <?php if ($id_especies == "2") { echo "selected"; } ?>>Gatxs</option>
														</select>
												
												</div>							
		
												<div class="col-12 col-sm-6">							
													  
														<label for='id_racas' style='display:block; '>Raça?</label>
														
														<select  class="form-control" name='id_racas' id='id_racas'>					
															
															<option value='0'>Não sei</option>

														</select>
												
												</div>							
					
												<div class="col-12 col-sm-6">															  
													  
															<label for='cep' style='display:block; '>Informe CEP de onde o animal se encontra:</label>
															<input type="text" name='cep' id='cep' class="form-control" placeholder="CEP" maxlength='9' value='<?=$cep?>'>
												</div>		  
													
												<input type='hidden' id='bairro' name='bairro' value=''>
												<input type='hidden' id='cidade' name='cidade' value=''>
												<input type='hidden' id='estado' name='estado' value=''>
													 		
												<div class="col-12 col-sm-6" id='divRespostaCep'>															  
													  
													
													 							
												</div>
											</div>	
											<div class="row" style='padding-top:10px'>			
												<div class="col-6">															  
														
														<label for='arquivo' style='display:block; '>Imagem adicionada:</label>
														
														<img src='<?=$imagem?>' id='imagemOld1'  style='width:300px; display:block'/>						
														<input type='hidden' id='imagemOld' name='imagemOld' value='<?=$imagem?>'>
														
														
												</div>
												<div class="col-6">															  
													  
															<label for='arquivo' style='display:block; '>Nova Imagem:</label>
																							
																					
														
															<input id="arquivo"  class='arquivo' type="file"  accept="image/*" name="arquivo">															
															<input type='hidden' id='imagem' name='imagem' value=''>
															
												</div>
												
											</div>	
											
											<div class="row" style='padding-top:10px'>		
												<div class="col-12">															  
													  
															<label for='descricao' style='display:block; '>Descreva o animal:</label>
														
																								  
													  
														<textarea  class="form-control" name='descricao' id='descricao' style='display:block; '><?=$descricao?></textarea>
													  
												</div>	
											</div>	


											<?php
											if ($id_status == 1){
											?>
											<div class="row" style='padding-top:10px'>		
												
												<div class="col-12 col-sm-4">															  
													  
													  <label for='sexo' style='display:block; '>Sexo?</label>
																					  
												  <select  class="form-control" name='sexo' id='sexo' style='display:block; '>					
													  
													  <option value='Não sei' <?php if ($sexo == "Não sei") { echo "selected"; } ?>>Não sei</option>
													  <option value='Macho' <?php if ($sexo == "Macho") { echo "selected"; } ?>>Macho</option>
													  <option value='Fêmea' <?php if ($sexo == "Fêmea") { echo "selected"; } ?>>Fêmea</option>
													  
												  </select>
																		   
										 	 	</div>	  
												<div class="col-12 col-sm-4">															  
													  
															<label for='castrado' style='display:block; '>Castrado?</label>
														
																					
														<select  class="form-control" name='castrado' id='castrado' style='display:block; '>					
															
															<option value='Não sei' <?php if ($castrado == "Não sei") { echo "selected"; } ?>>Não sei</option>
															<option value='Sim' <?php if ($castrado == "Sim") { echo "selected"; } ?>>Sim</option>
															<option value='Não' <?php if ($castrado == "Não") { echo "selected"; } ?>>Não</option>										
														</select>
													  							
												</div>
													  
												<div class="col-12 col-sm-4">															  
													  
															<label for='pedigree' style='display:block; '>Pedigree?</label>
														
																					
														<select  class="form-control" name='pedigree' id='pedigree' style='display:block; '>					
															
															<option value='Não sei' <?php if ($pedigree == "Não sei") { echo "selected"; } ?>>Não sei</option>
															<option value='Sim'<?php if ($pedigree == "Sim") { echo "selected"; } ?>>Sim</option>
															<option value='Não' <?php if ($pedigree == "Não") { echo "selected"; } ?>>Não</option>										
														</select>
													  							
												</div>
															  
												<div class="col-12 col-sm-4">															  
													  
															<label for='vermifugado' style='display:block; '>Vermifugado?</label>
														
																									  
													  
														<select  class="form-control" name='vermifugado' id='vermifugado' style='display:block; '>					
															
															<option value='Não sei' <?php if ($vermifugado == "Não sei") { echo "selected"; } ?>>Não sei</option>
															<option value='Sim' <?php if ($vermifugado == "Sim") { echo "selected"; } ?>>Sim</option>
															<option value='Não' <?php if ($vermifugado == "Não") { echo "selected"; } ?>>Não</option>
															
														</select>
													  
												</div>	
		  
												<div class="col-12 col-sm-4">															  
													  
															<label for='vacinado' style='display:block; '>Vacinado?</label>
														
																										  
													  
														<select  class="form-control" name='vacinado' id='vacinado' style='display:block; '>					
															
															<option value='Não sei' <?php if ($vacinado == "Não sei") { echo "selected"; } ?>>Não sei</option>
															<option value='Sim' <?php if ($vacinado == "Sim") { echo "selected"; } ?>>Sim</option>
															<option value='Não' <?php if ($vacinado == "Não") { echo "selected"; } ?>>Não</option>
															
														</select>
													  
												</div>
				  
												<div class="col-12 col-sm-4">															  
													  
															<label for='idade' style='display:block; '>Idade?</label>
														
																									  
													  
														<select  class="form-control" name='idade' id='idade' style='display:block; '>					
															<option value='Não sei'>Não sei</option>
															<option value='1 a 6 meses'>1 a 6 meses</option>
															<option value='6 meses a 1 ano'>6 meses a 1 ano</option>
															<option value='1 a 2 anos'>1 a 2 anos</option>
															<option value='2 a 3 anos'>2 a 3 anos</option>
															<option value='3 a 4 anos'>3 a 4 anos</option>
															<option value='5 ou mais anos'>5 ou mais anos</option>
															
														</select>
													  
												</div>		  
											</div>	
											<div class="row" style='padding-top:10px'>		
												<div class="col-12">	  
																					  
													  
													  
													  
													
													<div class="text-center">
													  
													  <button type='button' id='btnDoar' name='btnDoar' onclick='EnviarForm()' class="btn  btn-primary btn-round btn-lg" style='float:left'>Atualizar</button>
													  <button type='button' id='btnExcluir' name='btnExcluir' onclick='ExcluirPet()' class="btn  btn-primary btn-round btn-lg" style='float:right'>Excluir</button>
													  
													</div>
																				 
													 
													 
													 
												
												</div>					  
											</div>
											<?php
											} elseif($id_status == 3){
											?>
											<div class="row" style='padding-top:10px'>		
												
												<div class="col-12 col-sm-4">															  
													  
													  <label for='sexo' style='display:block; '>Sexo?</label>
																					  
												  <select  class="form-control" name='sexo' id='sexo' style='display:block; '>					
													  
													  <option value='Não sei' <?php if ($sexo == "Não sei") { echo "selected"; } ?>>Não sei</option>
													  <option value='Macho' <?php if ($sexo == "Macho") { echo "selected"; } ?>>Macho</option>
													  <option value='Fêmea' <?php if ($sexo == "Fêmea") { echo "selected"; } ?>>Fêmea</option>
													  
												  </select>
																		   
										 	 	</div>	  
												
				  
												<div class="col-12 col-sm-4">															  
													  
															<label for='idade' style='display:block; '>Idade?</label>
														
																									  
													  
														<select  class="form-control" name='idade' id='idade' style='display:block; '>					
															<option value='Não sei'>Não sei</option>
															<option value='1 a 6 meses'>1 a 6 meses</option>
															<option value='6 meses a 1 ano'>6 meses a 1 ano</option>
															<option value='1 a 2 anos'>1 a 2 anos</option>
															<option value='2 a 3 anos'>2 a 3 anos</option>
															<option value='3 a 4 anos'>3 a 4 anos</option>
															<option value='5 ou mais anos'>5 ou mais anos</option>
															
														</select>
													  
												</div>		  
											</div>	
											<div class="row" style='padding-top:10px'>		
												<div class="col-12">	  
																					  
													  
													  
													  
													
													<div class="text-center">
													  
													  <button type='button' id='btnDoar' name='btnDoar' onclick='EnviarForm()' class="btn  btn-primary btn-round btn-lg" style='float:left'>Atualizar</button>
													  <button type='button' id='btnExcluir' name='btnExcluir' onclick='ExcluirPet()' class="btn  btn-primary btn-round btn-lg" style='float:right'>Excluir</button>
													  
													</div>
																				 
													 
													 
													 
												
												</div>					  
											</div>						  
											<?php
											} elseif($id_status == 6){
											?>
											<div class="row" style='padding-top:10px'>		
												
												<div class="col-12 col-sm-4">															  
													  
													  <label for='sexo' style='display:block; '>Sexo?</label>
																					  
												  <select  class="form-control" name='sexo' id='sexo' style='display:block; '>					
													  
													  <option value='Não sei' <?php if ($sexo == "Não sei") { echo "selected"; } ?>>Não sei</option>
													  <option value='Macho' <?php if ($sexo == "Macho") { echo "selected"; } ?>>Macho</option>
													  <option value='Fêmea' <?php if ($sexo == "Fêmea") { echo "selected"; } ?>>Fêmea</option>
													  
												  </select>
																		   
										 	 	</div>	  
												
				  
												<div class="col-12 col-sm-4">															  
													  
															<label for='idade' style='display:block; '>Idade?</label>
														
																									  
													  
														<select  class="form-control" name='idade' id='idade' style='display:block; '>					
															<option value='Não sei'>Não sei</option>
															<option value='1 a 6 meses'>1 a 6 meses</option>
															<option value='6 meses a 1 ano'>6 meses a 1 ano</option>
															<option value='1 a 2 anos'>1 a 2 anos</option>
															<option value='2 a 3 anos'>2 a 3 anos</option>
															<option value='3 a 4 anos'>3 a 4 anos</option>
															<option value='5 ou mais anos'>5 ou mais anos</option>
															
														</select>
													  
												</div>		  
											</div>	
											<div class="row" style='padding-top:10px'>		
												<div class="col-12">	  
																					  
													  
													  
													  
													
													<div class="text-center">
													  
													  <button type='button' id='btnDoar' name='btnDoar' onclick='EnviarForm()' class="btn  btn-primary btn-round btn-lg" style='float:left'>Atualizar</button>
													  <button type='button' id='btnExcluir' name='btnExcluir' onclick='ExcluirPet()' class="btn  btn-primary btn-round btn-lg" style='float:right'>Excluir</button>
													  
													</div>
																				 
													 
													 
													 
												
												</div>					  
											</div>	
											<?php
											}
											
											?>
							  
							  
							  
							
						</div>				
						</form> 
					</div>
                 
                    
                   
                    
                  </div>
                  
						               
						  
                </div>




				<?php
                require("../../inc/rodape2.php");
                ?>
 



                
              </div>
			
			
            
            
            </div>
         
       
                       
              
                     
              
              
           </div>
         

 
  <!--   Core JS Files   -->
  <?php
 require("../../inc/scripts2.php") ;
  ?>
  <script>
  $(document).ready(function() {
		$("#cep").mask("99999-999"); 
		var files;
		// Add events
		$('input[type=file]').on('change', enviaArquivo);  
		buscaEndereco($('#cep').val());
	
	});

	function ExcluirPet(){
		bootbox.hideAll()
			
			
			var codigo = $("#pet").val();
			var dialog = bootbox.dialog({ 
				title: 	'Confirmação:', 
				message: 'Confirma a exclusão deste pet?',
				className: "modal-normal",
				buttons: {
					
					ok: {
					  label: "Sim",
					  className: "btn-modal-sucesso",
					  callback: function() {
						
						$.get("process.php?operacao=excluir&pet="+codigo,
						   function(data){
							   alert(data)
							   if(data > 0){ // 1 ou maior 
								   document.location.href = "./";
							   } else { //erro
								   tituloMensagem = "Erro:";
								   tipo = 'erro';
								   mensagem = "N&atilde;o foi poss&iacute;vel excluir o pet.";
								   dialog.close();
								   alerta(tituloMensagem, mensagem);
							   }
							   
							   
							 //alert("Data Loaded: " + data);
						   });	
						
						
					  }
					},
					ok2: {
					  label: "Não",
					  className: "btn-modal-sucesso",
					  callback: function() {
						dialog.modal('hide');
					  }
					}
				}
			});		
	
	}
function EnviarForm(){
	
	var err = '';

	var quantidadeErros = 0;

	var mensagem = '';

	var tipoMensagem = '';


	tipoMensagem = 'inserido';

	tipoMensagem2 = 'inserir';
	
	if ($('#id_especies').val() == ''){
		quantidadeErros++;
		mensagem += '- Espécie (obrigatório)<br />'
	}	
	if ($('#cep').val() == ''){
		quantidadeErros++;
		mensagem += '- CEP onde o pet se encontra (obrigatório)<br />'
	}
	if ($('#cidade').val() == '' || $('#estado').val() == '' || $('#bairro').val() == ''){
		quantidadeErros++;
		mensagem += '- CEP onde o pet se encontra é inválido<br />'
	}	
	
	if ($('#imagemOld').val() == '' && $('#imagem').val() == ''){
		quantidadeErros++;
		mensagem += '- Imagem (obrigatório)<br />'
	}

	//quantidadeErros++;

	if (quantidadeErros>0){

		if (quantidadeErros>1){

			err = 'Foram encontrados os seguintes erros:';
		
		} else {

			err = 'Foi encontrado o seguinte erro:';

			
		}

		
		//bootbox.hideAll();

		var dialog = bootbox.dialog({ 
			title: err, 
			message: mensagem,
			buttons: {
				
				ok: {
				  label: "OK",
				  className: "btn-primary",
				  callback: function() {
					dialog.modal('hide');
				  }
				}
			}
		});	
	
			
		

	 } else {
		

		$.post('process.php',$('#formPrincipal').serialize(),
		
		function(data){					   
							
			if(data > 0){ // 1 ou maior 
							
				tituloMensagem = 'Sucesso:';
								
				tipo = 'sucesso';
							
				mensagem = 'Pet atualizado com sucesso.';													
					
		
				var dialog = bootbox.dialog({ 
					title: tituloMensagem, 
					message: mensagem,
					buttons: {
												
						listar: {
						  label: "OK",
						  className: "btn-primary",
						  callback: function() {
							document.location.href = './';
						  }
						}
					},					
					onEscape: function() {
						document.location.href = './';
					}
				});
				
						
					
					
							
			} else { //erro
							
				tituloMensagem = 'Erro:';
							
				tipo = 'erro';
							
				mensagem = 'Não foi possível atualizar o pet.';
							
					
					bootbox.hideAll()
					var dialog = bootbox.dialog({ 
						title: tituloMensagem, 
						message: mensagem,
						buttons: {
							
							ok: {
							  label: "Ok",
							  className: "btn-primary",
							  callback: function() {
								dialog.modal('hide');
							  }
							}
						},					
						onEscape: function() {
							dialog.modal('hide');
						}		  
							
					});
			}
							
			
							
		});					 
	}
}		  
		  
	  

function buscaEndereco(cep){
		$.get( "https://brasilapi.com.br/api/cep/v1/"+cep, function( data ) {
			//$( ".result" ).html( data );
			//alert( data.state +  " - "  + data.city + " - " + data.neighborhood );
		
		
			$('#bairro').val(data.neighborhood);
			$('#cidade').val(data.city);
			$('#estado').val(data.state);
		
			$('#divRespostaCep').html("<div style='padding-top:40px'>" + data.neighborhood +  ", "  + data.city + " - " + data.state + "</div>");			
		}, "json" )  .fail(function() {
					$('#bairro').val('');
					$('#cidade').val('');
					$('#estado').val('');
					$('#divRespostaCep').html("");
					bootbox.hideAll()
					var dialog = bootbox.dialog({ 
						title: "Erro", 
						message: "Não foi possível localizar este CEP, por favor utilize outro.",
						buttons: {
							
							ok: {
							  label: "Ok",
							  className: "btn-primary",
							  callback: function() {
								dialog.modal('hide');
							  }
							}
						},					
						onEscape: function() {
							dialog.modal('hide');
						}		  
							
					});
		});
					  			  
		
	}	
	$( "#cep" ).change(function() {
		
		//if ($('#cep').val().length >= 8){
			buscaEndereco($('#cep').val())
		//	alert($('#cep').val()) 
		//} else {

			
		//}
	});
	
	function mudaRaca(especie){
				  
			   var especie = $('#id_especies :selected').val();
			   
			  $('#divRacas').load('racas.php?especie='+especie);
			
	}		
	$('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
	  var url = $(this).attr("href"); // the remote url for content
	  var target = $(this).data("target"); // the target pane
	  var tab = $(this); // this tab
	  
	  // ajax load from data-url
	  $(target).load(url,function(result){      
		tab.tab('show');
	  });
	});

	// initially activate the first tab..
	//$('#tabIni').click();  
	
	//$('').load(url,function(result){      
	//	tab.tab('show');
	 // });
	 
	function enviaArquivo(){

		$.ajax( {
		  url: 'process.php?operacao=inserir_arquivo',
		  type: 'POST',
		  data: new FormData($("#formPrincipal")[0]),
		  processData: false,
		  success: function( data, textStatus ){		  

				if (data != "0"){ // sucesso					
				   
					$( "#imgOld" ).remove();
					$( "#imagemOld" ).remove();
					//$( "#imagemOld1" ).remove();
					$( "#imagem" ).after( "<img src='../../uploads/tmp/"+data+"' id='imgOld' width='300px' style='margin:0 auto; display:block' />" );
				
					
							
					$( "#imagem" ).val(data);
				} else {

					bootbox.hideAll()
					var dialog = bootbox.dialog({ 
						title: "Erro", 
						message: "Não foi possível carregar a imagem, por favor utilize outra.",
						buttons: {
							
							ok: {
							  label: "Ok",
							  className: "btn-primary",
							  callback: function() {
								dialog.modal('hide');
							  }
							}
						},					
						onEscape: function() {
							dialog.modal('hide');
						}		  
							
					});
					
				}
			  
		  },
		  contentType: false
		});
		
	}	 
	  
	 
  </script>  
</body>

</html>
