-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Tempo de geração: 03-Jun-2021 às 19:02
-- Versão do servidor: 5.7.32
-- versão do PHP: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `encontrapet`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `dialogos_pessoas_pets`
--

CREATE TABLE `dialogos_pessoas_pets` (
  `id` int(11) NOT NULL,
  `id_pets` int(11) NOT NULL,
  `id_pessoas` int(11) NOT NULL,
  `data_hora` datetime DEFAULT NULL,
  `texto` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `especies`
--

CREATE TABLE `especies` (
  `id` int(11) NOT NULL,
  `tipo` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `especies`
--

INSERT INTO `especies` (`id`, `tipo`) VALUES
(1, 'Cachorrxs'),
(2, 'Gatxs');

-- --------------------------------------------------------

--
-- Estrutura da tabela `fotos`
--

CREATE TABLE `fotos` (
  `id` int(11) NOT NULL,
  `id_pets` int(11) NOT NULL,
  `legenda` varchar(140) DEFAULT NULL,
  `caminho` varchar(50) DEFAULT NULL,
  `data` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `interacoes_pets`
--

CREATE TABLE `interacoes_pets` (
  `id` int(11) NOT NULL,
  `id_pets` int(11) NOT NULL,
  `id_pessoas` int(11) NOT NULL,
  `data_hora` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `movimentacoes_pets`
--

CREATE TABLE `movimentacoes_pets` (
  `id` int(11) NOT NULL,
  `id_pets` int(11) NOT NULL,
  `id_status` int(11) NOT NULL,
  `data_hora` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `pessoas`
--

CREATE TABLE `pessoas` (
  `id` int(11) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `face_id` varchar(100) DEFAULT NULL,
  `telefone` varchar(20) DEFAULT NULL,
  `senha` varchar(100) NOT NULL,
  `id_pessoas_tipos` int(1) NOT NULL DEFAULT '1',
  `data_cadastro` datetime NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1' COMMENT '1 - ativo, 2 - excluido'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `pessoas_enderecos`
--

CREATE TABLE `pessoas_enderecos` (
  `id_pessoas` int(11) NOT NULL DEFAULT '0',
  `cep` varchar(13) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bairro` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `endereco` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `numero` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `complemento` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `uf` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cidade` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `pessoas_tipos`
--

CREATE TABLE `pessoas_tipos` (
  `id` int(11) NOT NULL,
  `tipo` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `pessoas_tipos`
--

INSERT INTO `pessoas_tipos` (`id`, `tipo`) VALUES
(1, 'Pessoas'),
(2, 'Ong'),
(3, 'Clinica Veterinaria'),
(4, 'Casa de Racao'),
(5, 'Petshop');

-- --------------------------------------------------------

--
-- Estrutura da tabela `pets`
--

CREATE TABLE `pets` (
  `id` int(11) NOT NULL,
  `id_pessoas` int(11) NOT NULL,
  `id_especies` int(11) NOT NULL,
  `nome` varchar(100) DEFAULT NULL,
  `descricao` text,
  `sexo` varchar(10) NOT NULL,
  `castrado` varchar(10) DEFAULT NULL,
  `idade` varchar(100) DEFAULT NULL,
  `id_status` int(11) NOT NULL,
  `vacinado` varchar(10) DEFAULT NULL,
  `vermifugado` varchar(10) DEFAULT NULL,
  `pedigree` varchar(100) DEFAULT NULL,
  `cor` varchar(100) DEFAULT NULL,
  `tamanho` varchar(100) DEFAULT NULL,
  `data_cadastro` datetime DEFAULT NULL,
  `id_racas` int(11) NOT NULL DEFAULT '0',
  `cep` varchar(10) DEFAULT NULL,
  `estado` varchar(50) DEFAULT NULL,
  `cidade` varchar(100) DEFAULT NULL,
  `bairro` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `racas`
--

CREATE TABLE `racas` (
  `id` int(11) NOT NULL,
  `id_especies` int(11) NOT NULL DEFAULT '0',
  `raca` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `status`
--

CREATE TABLE `status` (
  `id` int(11) NOT NULL,
  `status` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `status`
--

INSERT INTO `status` (`id`, `status`) VALUES
(1, 'Pra adocao'),
(2, 'Adotado'),
(3, 'Perdido'),
(4, 'Achado'),
(5, 'Excluido'),
(6, 'Encontrado'),
(7, 'Entregue(encontrado)');

--
-- Índices para tabelas despejadas
--

--
-- Índices para tabela `dialogos_pessoas_pets`
--
ALTER TABLE `dialogos_pessoas_pets`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_pets` (`id_pets`),
  ADD KEY `id_pessoas` (`id_pessoas`);

--
-- Índices para tabela `especies`
--
ALTER TABLE `especies`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `fotos`
--
ALTER TABLE `fotos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_pets` (`id_pets`);

--
-- Índices para tabela `interacoes_pets`
--
ALTER TABLE `interacoes_pets`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_pets` (`id_pets`),
  ADD KEY `id_pessoas` (`id_pessoas`);

--
-- Índices para tabela `movimentacoes_pets`
--
ALTER TABLE `movimentacoes_pets`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_pets` (`id_pets`),
  ADD KEY `id_status` (`id_status`);

--
-- Índices para tabela `pessoas`
--
ALTER TABLE `pessoas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `email` (`email`);

--
-- Índices para tabela `pessoas_enderecos`
--
ALTER TABLE `pessoas_enderecos`
  ADD KEY `id_pessoas` (`id_pessoas`);

--
-- Índices para tabela `pessoas_tipos`
--
ALTER TABLE `pessoas_tipos`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `pets`
--
ALTER TABLE `pets`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_especies` (`id_especies`),
  ADD KEY `id_pessoas` (`id_pessoas`),
  ADD KEY `id_status` (`id_status`),
  ADD KEY `vacinado` (`vacinado`),
  ADD KEY `vermifugado` (`vermifugado`),
  ADD KEY `castrado` (`castrado`),
  ADD KEY `sexo` (`sexo`);

--
-- Índices para tabela `racas`
--
ALTER TABLE `racas`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `status`
--
ALTER TABLE `status`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `dialogos_pessoas_pets`
--
ALTER TABLE `dialogos_pessoas_pets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `especies`
--
ALTER TABLE `especies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de tabela `fotos`
--
ALTER TABLE `fotos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `interacoes_pets`
--
ALTER TABLE `interacoes_pets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `movimentacoes_pets`
--
ALTER TABLE `movimentacoes_pets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `pessoas`
--
ALTER TABLE `pessoas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `pessoas_tipos`
--
ALTER TABLE `pessoas_tipos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de tabela `pets`
--
ALTER TABLE `pets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `racas`
--
ALTER TABLE `racas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `status`
--
ALTER TABLE `status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
