<?php 
ob_start();
session_start();

/**
*	@autor Flávio Silva Brandão <flavio@afixo.com.br>
*	@version 21-01-2020 as 09:00:30
*/

/*
 * verificando se o usuario esta logado senao redireciona
 * */
if (!$_SESSION['ENCONTRAPET_codigo']){
	header("Location: ../splash");
}
# 
# REQUIRES OBRIGATORIOS
#


require('../classes/gestor.php');

#
# INSTANCIA DE OBJETOS OBRIGATORIAS
#


$util = new Util();
$gestor   = new Gestor();    

#
# TRATAMENTO DE INJECTION
#

$_POST = $util->validaParametro($_POST);
$_GET  = $util->validaParametro($_GET);









?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
  <link rel="icon" type="image/png" href="../assets/img/favicon.png">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    EncontraPet - Início
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <?php
  require("../inc/css.php");
  ?>
  
  <style>
      .map {
        height: 300px;
        width: 100%;
      }
    </style>
</head>

<body class="text-center" style=''>


              
       
              

            <div class="" style='padding:0'>      
				
				<?php
				require("../inc/navegacao.php")
				?>
            

              <!-- Nav tabs -->
              <div class="container-fluid">
                
                
                <div class='row' style='padding-top:20px'>
                  <div class='col-12'>
                    <div>
                      <a class="card-title" href="../procurar/index.php"  style='color:#333; text-decoration:none; float:left'><i class="fa fa-angle-double-left"></i> Retornar</a>
                      <div class='col-12 text-right'>
                      
                        <a class="card-title" data-toggle="collapse" href="#divFiltros" role="button" aria-expanded="false" aria-controls="divFiltros" style='color:#333; text-decoration:none'><i class="fa fa-search"></i> Filtros </a>
                        
                      </div>
                    </div>
                  </div>

                  <div class="collapse col-12" id="divFiltros">	

                    <form id='frmFiltro' method="POST" name='frmFiltro' enctype="multipart/form-data">
                    <div class='row' style='padding-bottom:20px'>					
                        <div class='col-8' style='padding-top:13px; '>	
                            <select class='form-control' id='id_especies' name='id_especies'>
                            <option  selected='true' disabled='disabled' value=''>Visualizar</option>
                            <option    value=''> Apenas Pets</option>
                            <option    value=''>Apenas Ongs</option>
                            <option    value=''>Pets, Empresas e Ongs</option>
                                
                            </select>
                        </div>
                        
                        <div class='col-4'>	

                            
                            <button class="btn btn-primary btn-round btn-lg" style='width:100%' type='button' onclick='filtrar()'><i class="fa fa-search"></i></button>
                        </div>
                    </div>

                    </form>			
                  </div>





                <div id="map" class="map"></div>
                  
                  

                  <div>
                    <p>Legendas:</p>
                  </div>
                
                
                </div>
				  
				       
				  

                  
						               
						  
                </div>




                <?php
                require("../inc/rodape.php");
                ?>



                
              </div>
			
			
            
            
            </div>
         
       
                       
              
                     
              
              
           </div>
         

 
  <!--   Core JS Files   -->
  <?php
 require("../inc/scripts.php") ;
  ?>
  <script async
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBk48dYwdGccm2TueN4j-DFBf_B6HGynoc&libraries=geometry">
  </script>







  <script>

      
      //let animalArray = [[-8.0412672,-34.939636,1],[-8.0371376,-34.9468692,3],[-8.065235,-34.941552,3]] ;
     

      function verificaSeSuportaGeolocalizacao() {
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(initMap);
        } else { 
          x.innerHTML = "Geolocation is not supported by this browser.";
        }
      }

      
      let map;
  
      

      const initMap = async (position) => {
      
      const geocoder = new google.maps.Geocoder();  

      let location = await getUserLocName(geocoder,position.coords.latitude, position.coords.longitude);
      
      let areaAnimals = [];
      let areaOrgs = [];
      
      //Carrega animais presentes na cidade do usuário  
      function loadAreaAnimals() {
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {
            areaAnimals = JSON.parse(this.response);
          }
        };
        xhttp.open("GET", "userLocHelper.php?City="+location, false);
        xhttp.send();
      }

      //Carrega empresas e ongs presentes na cidade do usuário
      function loadAreaOrgs() {
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {
            areaOrgs = JSON.parse(this.response);
          }
        };
        xhttp.open("GET", "userLocOrgsHelper.php?City="+location, false);
        xhttp.send();
      }


      //loadAreaAnimals();
      //loadAreaOrgs();
     

      //coleta as coordenadas dos animais
      for (let i = 0; i < areaAnimals.length; i++) {
        let petLatLng = await getPetsCoords(geocoder,areaAnimals[i]['cidade'],areaAnimals[i]['rua']);
        areaAnimals[i].lat = petLatLng.lat();
        areaAnimals[i].lng = petLatLng.lng();
      }



        //coleta as coordenadas das ongs - OBS refatorar para coords serem coletadas em momento de cadastro
       for (let i = 0; i < areaOrgs.length; i++) {
        let orgLatLng = await getOrgsCoords(geocoder,areaOrgs[i]['cidade'],areaOrgs[i]['endereco'],areaOrgs[i]['numero']);
        areaOrgs[i].lat = orgLatLng.lat();
        areaOrgs[i].lng = orgLatLng.lng();
      }
    

        map = new google.maps.Map(document.getElementById("map"), {
          center: { lat: position.coords.latitude, lng: position.coords.longitude },
          zoom: 14,
          zoomControl: true,
          disableDefaultUI: true
        });

        map.addListener("click", (mapsMouseEvent)=>{

          let position = mapsMouseEvent.latLng;
          
          //console.log(position.lat());

            cityCircle = new google.maps.Circle({
              strokeColor: '#77a6b2',
              strokeOpacity: 0.1,
              strokeWeight: 2,
              fillColor: '#e9e7f9',
              fillOpacity: 0.3,
              map: map,
              center: position,
              radius: (10000) 
              });    

              loadMapMarkers(map,areaAnimals,areaOrgs,position);

        });

        let kCord =  (position.coords.latitude);
        let pCord =  (position.coords.longitude);         
        let Cord = new google.maps.LatLng(kCord, pCord);

        let cityCircle = new google.maps.Circle({
            strokeColor: '#77a6b2',
            strokeOpacity: 0,
            strokeWeight: 2,
            fillColor: '#e9e7f9',
            fillOpacity: 0,
            map: map,
            center: {lat: kCord, lng: pCord},
            radius: (10000) 
        });
        //Validar pontos abaixo:

        //Limitar um range para cidade inteira!  = Aumentado de 2km para 10 Km
        //validar se range pode ficar invisivel! Radius existe mas não é exibido em tela = Deixar sem que o user entenda que existe um limite de área.
        //Limite de 1 circulo a cada request? = Ao clicar num proximo range o atual deve ser descartado?
        //Preview de pins no mapa, visão de quais resultados estão vindo  (same as "Nearby me" Maps api) - 


        //getUserLocationNames(geocoder,position.coords.latitude, position.coords.longitude );
        loadMapMarkers(map,areaAnimals,areaOrgs,Cord);
        
      }


      const getPetsCoords = (geocoder, city,street) =>{
        const address = city+","+street;
  
        return new Promise((resolve,reject)=>{
          geocoder.geocode({address: address},(results,status)=>{
            if (status === 'OK'){
              resolve(results[0].geometry.location)
            }else{
              reject(status)
            }
          });
        }); 
      };



      const getOrgsCoords = (geocoder, city,street,number) =>{
        const address = city+","+street;
  
        return new Promise((resolve,reject)=>{
          geocoder.geocode({address: address},(results,status)=>{
            if (status === 'OK'){
              resolve(results[0].geometry.location)
            }else{
              reject(status)
            }
          });
        }); 
      };


      const getUserLocName = (geocoder, inputLat,inputLng) =>{
        const latLng = {
          lat: inputLat,
          lng: inputLng
        }
        return new Promise((resolve,reject)=>{
          geocoder.geocode({location: latLng }, (results, status)=>{
            if(status ==='OK'){
              resolve(results[0].address_components[3].long_name)
            }else{
              reject(status);
            }
          });
        });
      };

  

      function loadMapMarkers(map, areaAnimals,areaOrgs,Cord ){
        let baseRadius = 12037; // Radius atual de busca, limitado a unidade em kilometros

        
        let markerInfo = [];
        let markerInfoOrg = [];    
        let nearbyAnimals = []; 
        let nearbyOrgs = [];
        let infowindow = new google.maps.InfoWindow();
        let image;
        let orgIcon;
        let status;

        for (let i = 0; i < areaAnimals.length; i++) {

          let lat = areaAnimals[i]['lat'];
          let long = areaAnimals[i]['lng'];
          let animalLoc = new google.maps.LatLng(lat, long);
          let distance = (google.maps.geometry.spherical.computeDistanceBetween(Cord, animalLoc));

          if(areaAnimals[i]['id_especies'] == 1){
            if (areaAnimals[i]['id_status'] == 3){
              status = "Perdido";
              image = "dogIconLost.png";
            }else{
              status = "Encontrado";
              image = "dogIcon.png";
            }
          }else{
            if (areaAnimals[i]['id_status'] == 3){
              status = "Perdido";
              image = "catIconLost.png";
            }else{
              status = "Encontrado";
              image = "catIcon.png";
            }
          }

       

          if (distance <= baseRadius){

             markerInfo[i] = 
              '<div id="content">' +
                '<div id="siteNotice">' +
                "</div>" +
                '<div id="bodyContent">' +
                "<span>Animal <b>"+status+"</b> "
                 + areaAnimals[i]['sexo']+
                "</br><span>Cadastrado em: "+areaAnimals[i]['data_cadastro']+
                "<a href=../procurar/pet/?pet="+areaAnimals[i]['id']+"></i> Visualizar</a>"+	
                "</div>" +
              "</div>";
            
    
            nearbyAnimals[i] = new google.maps.Marker({
              map,
              animation: google.maps.Animation.DROP,
              //animation: google.maps.Animation.BOUNCE,
              icon:image,
              position: {lat: Number(areaAnimals[i]['lat']), lng: Number(areaAnimals[i]['lng'])}
            });

            nearbyAnimals[i].addListener("click", () => {
              infowindow.setContent( markerInfo[i]);
              infowindow.open({
                anchor: nearbyAnimals[i],
                map,
                shouldFocus: false,
              });
            });

            } 
        }


        for (let i = 0; i < areaOrgs.length; i++) {

          let lat = areaOrgs[i]['lat'];
          let long = areaOrgs[i]['lng'];
          let OrgLoc = new google.maps.LatLng(lat, long);
          let distance = (google.maps.geometry.spherical.computeDistanceBetween(Cord, OrgLoc));

          if(areaOrgs[i]['id_pessoas_tipos'] == 2 || 4){
            orgIcon = "home.png";
          }



          if (distance <= baseRadius){

            markerInfoOrg[i] = '';
            

            nearbyOrgs[i] = new google.maps.Marker({
              map,
              animation: google.maps.Animation.DROP,
              //animation: google.maps.Animation.BOUNCE,
              icon:orgIcon,
              position: {lat: Number(areaOrgs[i]['lat']), lng: Number(areaOrgs[i]['lng'])}
            });

            nearbyOrgs[i].addListener("click", () => {
              infowindow.setContent( markerInfoOrg[i]);
              infowindow.open({
                anchor: nearbyOrgs[i],
                map,
                shouldFocus: false,
              });
            });

            } 
        }

      }

     


      $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
        var url = $(this).attr("href"); // the remote url for content
        var target = $(this).data("target"); // the target pane
        var tab = $(this); // this tab
        
        // ajax load from data-url
        $(target).load(url,function(result){      
        tab.tab('show');
        });
      });

          jQuery(document).ready(function(){
        
        
            verificaSeSuportaGeolocalizacao()
        
          });	
  </script>  

</body>

</html>

