<?php 
ob_start();
session_start();
/**
*
*	@autor Flávio Silva Brandão <flavio@afixo.com.br>
*	@version 26-05-2021 as 12:06:55
*/



# 
# REQUIRES OBRIGATORIOS
#

require('../../classes/gestor.php');

$util = new Util();
$gestor   = new Gestor();

#
# TRATAMENTO DE INJECTION
#

$_POST = $util->validaParametro($_POST);

extract($_POST);
extract($_GET);

# 
# VARIAVEIS  
# 
if ($operacao != "excluir"){
	$senha = $util->encripty($senha);	
	$face_id = "";
	$objeto = array(array('id'=>$_SESSION['ENCONTRAPET_codigo'],'nome'=>$nome,'email'=>$email,'face_id'=>$face_id,'telefone'=>$telefone,'senha'=>$senha));



	$objPessoas = $gestor->retornarPessoas($email,'email');
	if ($objPessoas){

		$obj = $gestor->editarPessoas($objeto);	
		//var_dump($obj);
		//die;	
		if ($obj){
				die('1');
		} else {
				die('0');
		}	
	} else {
		die('0');
	}	
} else {
	$objeto = array(array('id'=>$_SESSION['ENCONTRAPET_codigo'],'status'=>'2'));



	$objPessoas = $gestor->retornarPessoas($_SESSION['ENCONTRAPET_codigo']);
	if ($objPessoas){

		$obj = $gestor->editarPessoas($objeto);
		if ($obj){
			$_SESSION = array();
			if (isset($_COOKIE[session_name()])) {
				setcookie(session_name(), '', time()-42000, '/');
			}
			session_destroy();	
			die('1');
		} else {
				die('0');
		}		
	} else {
		die('0');
	}	

}
?>
