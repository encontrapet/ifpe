<?php 
ob_start();
session_start();

/**
*	@autor Flávio Silva Brandão <flavio@afixo.com.br>
*	@version 21-01-2020 as 09:00:30
*/

/*
 * verificando se o usuario esta logado senao redireciona
 * */
if (!$_SESSION['ENCONTRAPET_codigo']){
	header("Location: ../splash");
}
# 
# REQUIRES OBRIGATORIOS
#


require('../classes/gestor.php');

#
# INSTANCIA DE OBJETOS OBRIGATORIAS
#


$util = new Util();
$gestor   = new Gestor();    

#
# TRATAMENTO DE INJECTION
#

$_POST = $util->validaParametro($_POST);
$_GET  = $util->validaParametro($_GET);
?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
  <link rel="icon" type="image/png" href="../assets/img/favicon.png">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    EncontraPet - Início
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <?php
  require("../inc/css.php");
  ?>
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/openlayers/openlayers.github.io@master/en/v6.5.0/css/ol.css" type="text/css">
  <style>
      .map {
        height: 400px;
        width: 100%;
      }
    </style>
</head>

<body class="text-center" style=''>


              
       
              

            <div class="" style='padding:0'>      
				
				<?php
				require("../inc/navegacao.php")
				?>
            

              <!-- Nav tabs -->
              <div class="container-fluid">
                
                
                <div class='row' style='padding-top:20px'>
                  <div class='col-12'>
                  
                  <div id="map" class="map"></div>
                  
                  </div>
                
                
                </div>
				  
				       
				  

                  
						               
						  
                </div>




                <?php
                require("../inc/rodape.php");
                ?>



                
              </div>
			
			
            
            
            </div>
         
       
                       
              
                     
              
              
           </div>
         

 
  <!--   Core JS Files   -->
  <?php
 require("../inc/scripts.php") ;
  ?>
  <script src="https://cdn.jsdelivr.net/gh/openlayers/openlayers.github.io@master/en/v6.5.0/build/ol.js"></script>
  <script>
  var map;
  var position;
  var markers;
  function verificaSeSuportaGeolocalizacao() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(setaPosicaoNoMapa);
    } else { 
      x.innerHTML = "Geolocation is not supported by this browser.";
    }
  }

  function setaPosicaoNoMapa(position) {
    var msg = "Latitude: " + position.coords.latitude + 
    "<br>Longitude: " + position.coords.longitude;
        

        //alert(msg)
          map = new ol.Map({
            target: 'map',
            numZoomLevels: 18,
            maxResolution: 156543.0339,
            layers: [
              new ol.layer.Tile({
                source: new ol.source.OSM()
              })
            ],
            view: new ol.View({
              center: ol.proj.fromLonLat([position.coords.longitude, position.coords.latitude]),
              zoom: 15
            })
          });

          posicao       = new OpenLayers.LonLat(position.coords.longitude, position.coords.latitude).transform( fromProjection, toProjection);
         
          markers = new OpenLayers.Layer.Markers( "Markers" );
          map.addLayer(markers);
          
          markers.addMarker(new OpenLayers.Marker(posicao));

  }
	$('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
	  var url = $(this).attr("href"); // the remote url for content
	  var target = $(this).data("target"); // the target pane
	  var tab = $(this); // this tab
	  
	  // ajax load from data-url
	  $(target).load(url,function(result){      
		tab.tab('show');
	  });
	});

      jQuery(document).ready(function(){
	  
	  
        verificaSeSuportaGeolocalizacao()
	  
       });	
  </script>  

</body>

</html>

