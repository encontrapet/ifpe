<?php

use PHPUnit\Framework\TestCase;


final class ServicosTest extends TestCase{

    /* Sutes de testes de Serviços(API) utilizando Guzzle para requisições a API e PHPUnit para validação*/
    /* Testes referentes aos dois recursos primários do sistema: Pets e Pessoas    */
    /* Futuramente será adicionado o recurso: Empresas, que por sua vez contará com seus testes neste arquivo  */

    
    /* Pessoas */

    public function testPessoasSuite(){

        $client = new GuzzleHttp\Client();
        $id = rand(100,8888);

        $queryparams = [
            'query' => [
               'id' => $id
            ]
         ];

        $paramsInsert = [
            'form_params' => [
               'id' => $id,
               'nome' => 'apiTest',
               'email' => 'apiTest@email.com',
                'senha' => 123,
                'telefone' => 12312312,
                'face_id' => 'fakepath/image.png',
                'especies_interesses' => 0,
                'id_municipios_interesses' => 0,
                'operacao' => 'inserir'
            ]
         ];

         $paramsEdit = [
            'form_params' => [
               'id' => $id,
               'nome' => 'apiTestEdited',
               'email' => 'apiTestEdited@email.com',
                'senha' => 123,
                'telefone' => 12312312,
                'face_id' => 'fakepath/image.png',
                'especies_interesses' => 0,
                'id_municipios_interesses' => 0,
                'operacao' => 'editar'
            ]
         ];

         $paramsDelete = [
            'form_params' => [
               'id' => $id,  
                'operacao' => 'excluir'
            ]
         ];



         /* Index */
        //Pesquisa os registros e caso a requisição tenha retorno (200 - ok) retorna a quantidade de registros

        $resIndex = $client->request('GET','http://localhost/api/pessoas/index.php');

        $this->assertEquals(200,$resIndex->getStatusCode());
        $dataIndex = json_decode($resIndex->getBody(true), true);
        print('Numero de registros');
        var_dump(sizeof($dataIndex));    



         /* Create */
         /* Cria o registro e caso a requisição tenha retorno (200 - ok) e que o ID retornado pela API seja o mesmo que foi passado na chamada printa ele em tela.*/     
        $resPost = $client->request('POST','http://localhost/api/pessoas/index.php', $paramsInsert);
        $this->assertEquals(200,$resPost->getStatusCode());
        $dataPost = json_decode($resPost->getBody(true), true);
        $this->assertEquals($id,$dataPost);
        print('Id de Registro Criado:');
        var_dump($dataPost);  



        /* Get */
        /* Busca pelo registro recém criado através de seu ID e caso a requisição tenha retorno (200 - ok ) printa ele em tela */
        $resGet = $client->request('GET','http://localhost/api/pessoas/index.php', $queryparams);
        $this->assertEquals(200,$resGet->getStatusCode());
        $dataGet = json_decode($resGet->getBody(true), true);
        print('Registro Pesquisado:');
        var_dump($dataGet); 
        
        
        /* Edit */
        /* Edita o registro recém criado  e verifica se a requisição teve status (200 - ok ) e se a API retornou codigo 1 de sucesso */
        $resEdit = $client->request('POST','http://localhost/api/pessoas/index.php', $paramsEdit);
        $this->assertEquals(200,$resEdit->getStatusCode());
        $dataEdit = json_decode($resEdit->getBody(true), true);
        $this->assertEquals(1,$dataEdit);

        /* Delete */
        /* Deleta o registro recém criado e verifica se a requisição teve status (200 - ok ) e se a API retornou codigo 1 de sucesso  */
        $resDelete = $client->request('POST','http://localhost/api/pessoas/index.php', $paramsDelete);
        $this->assertEquals(200,$resDelete->getStatusCode());
        $dataDelete = json_decode($resDelete->getBody(true), true);
        $this->assertEquals(1,$dataDelete);
        
    }




    /* Pets */

    public function testPetsSuite(){
        $client = new GuzzleHttp\Client();
        $id = rand(100,8888);

        $queryparams = [
            'query' => [
               'id' => $id
            ]
         ];

        $paramsInsert = [
            'form_params' => [
               'id' => $id,
               'id_pessoas' => 1,
               'id_especies' => 1,
               'nome' => 'petTest',
               'descricao' => 'TestAnimal',
                'sexo' => 'Macho',
                'idade' => 'Não sei',
                'id_status' => 0,
                'responsavel' => 'User',
                'telefone' => '99999999',
                'id_municipios' => 1,
                'operacao' => 'inserir'
                
            ]
         ];

         $paramsEdit = [
            'form_params' => [
               'id' => $id,
               'id_pessoas' => 1,
               'id_especies' => 1,
               'nome' => 'petTestEdited',
               'descricao' => 'TestAnimalEdited',
                'sexo' => 'Macho',
                'idade' => 'Não sei',
                'id_status' => 0,
                'responsavel' => 'User',
                'telefone' => '99999999',
                'id_municipios' => 1,
                'operacao' => 'editar'
                
            ]
         ];

         $paramsDelete = [
            'form_params' => [
               'id' => $id,  
                'operacao' => 'excluir'
            ]
         ];



         /* Index */
        //Pesquisa os registros e caso a requisição tenha retorno (200 - ok) retorna a quantidade de registros

        $resIndex = $client->request('GET','http://localhost/api/pets/index.php');

        $this->assertEquals(200,$resIndex->getStatusCode());
        $dataIndex = json_decode($resIndex->getBody(true), true);
        print('Numero de registros');
        var_dump(sizeof($dataIndex));    



         /* Create */
         /* Cria o registro e caso a requisição tenha retorno (200 - ok) e que o ID retornado pela API seja o mesmo que foi passado na chamada printa ele em tela.*/     
        $resPost = $client->request('POST','http://localhost/api/pets/index.php', $paramsInsert);
        $this->assertEquals(200,$resPost->getStatusCode());
        $dataPost = json_decode($resPost->getBody(true), true);
        $this->assertEquals($id,$dataPost);
        print('Id de Registro Criado:');
        var_dump($dataPost);  



        /* Get */
        /* Busca pelo registro recém criado através de seu ID e caso a requisição tenha retorno (200 - ok ) printa ele em tela */
        $resGet = $client->request('GET','http://localhost/api/pets/index.php', $queryparams);
        $this->assertEquals(200,$resGet->getStatusCode());
        $dataGet = json_decode($resGet->getBody(true), true);
        print('Registro Pesquisado:');
        var_dump($dataGet); 
        
        
        /* Edit */
        /* Edita o registro recém criado  e verifica se a requisição teve status (200 - ok ) e se a API retornou codigo 1 de sucesso */
        $resEdit = $client->request('POST','http://localhost/api/pets/index.php', $paramsEdit);
        $this->assertEquals(200,$resEdit->getStatusCode());
        $dataEdit = json_decode($resEdit->getBody(true), true);
        $this->assertEquals(1,$dataEdit);

        /* Delete */
        /* Deleta o registro recém criado e verifica se a requisição teve status (200 - ok ) e se a API retornou codigo 1 de sucesso  */
        $resDelete = $client->request('POST','http://localhost/api/pets/index.php', $paramsDelete);
        $this->assertEquals(200,$resDelete->getStatusCode());
        $dataDelete = json_decode($resDelete->getBody(true), true);
        $this->assertEquals(1,$dataDelete);
        
    }







     



       

  
} 



?>