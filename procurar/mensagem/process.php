<?php 
ob_start();
session_start();
/**
*
*	@autor Flávio Silva Brandão <flavio@afixo.com.br>
*	@version 03-06-2021 as 18:06:55
*/



# 
# REQUIRES OBRIGATORIOS
#

require('../../classes/gestor.php');

$util = new Util();
$gestor   = new Gestor();

#
# TRATAMENTO DE INJECTION
#

$_GET = $util->validaParametro($_GET);

extract($_GET);
extract($_POST);
# 
# VARIAVEIS  
# 



//var_dump($_GET);
//die;
if ($operacao == "excluir"){
	$obj = $gestor->excluirDialogosPessoasPets($msg);		
	if ($obj){		
			die('1');
	} else {
			die('0');
	}	
} else {
	$objeto = array(array('texto'=>addslashes($mensagem),'id_pets'=>$pet,'id_pessoas'=>$_SESSION['ENCONTRAPET_codigo'],'data_hora'=>date('Y-m-d H:i:s')));

	$obj = $gestor->inserirDialogosPessoasPets($objeto);		
	if ($obj){		
			die('1');
	} else {
			die('0');
	}	
}
	
?>
