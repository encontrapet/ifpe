<?php 
ob_start();
session_start();

/**
*	@autor Flávio Silva Brandão <flavio@afixo.com.br>
*	@version 21-01-2020 as 09:00:30
*/

/*
 * verificando se o usuario esta logado senao redireciona
 * */
if (!$_SESSION['ENCONTRAPET_codigo']){
	header("Location: ../../splash");
}

# 
# REQUIRES OBRIGATORIOS
#


require('../../classes/gestor.php');

#
# INSTANCIA DE OBJETOS OBRIGATORIAS
#


$util = new Util();
$gestor   = new Gestor();    

#
# TRATAMENTO DE INJECTION
#

$_POST = $util->validaParametro($_POST);
$_GET  = $util->validaParametro($_GET);

extract($_GET);
/*
 * 
 * reuperar os pets cadastrados anteriormente */
if (!$pet){
	header("location:../");
}
$objetoPets = $gestor->retornarPets($pet);
if ($objetoPets){
	if (@count($objetoPets) < 2){
		$objetoPets = array($objetoPets);
	}
} else {

	header("location:./");
}

//var_dump($objetoPets);
//die;
?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="../../assets/img/apple-icon.png">
  <link rel="icon" type="image/png" href="../../assets/img/favicon.png">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    EncontraPet - Início
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <?php
  require("../../inc/css2.php");
  ?>
  
<style type="text/css">

</style>
</head>

<body class="text-center" style=''>


              
       
              

            <div class="" style='padding:0'>      
				
				<?php
				require("../../inc/navegacao2.php")
				?>
            

              <!-- Nav tabs -->
              <div style="">
				  
				       
				  

                <div class="" style='padding-left:3px; padding-right:3px; padding-top:5px; '>
                  <!-- Tab panes -->
                  <div class="tab-content text-center">

					  
                    <div class="tab-pane active" id="pets" role="tabpanel">
						<div class='container'>
						<?php
						foreach($objetoPets as $obj){
							
							
							$imagem = $gestor->retornarPets($obj->id,'id_pets','fotos','id','DESC',1)->caminho;
							$dataHora = $gestor->retornarMovimentacoesPets(array($obj->id,1),array('id_pets','id_status'),'movimentacoes_pets','id','DESC',1)->data_hora;
							
							if (!$imagem){
								if ($obj->id_especies == 1){ // cachorros
									$imagem = '../../assets/img/pets/black-dog.png'; 
								} else {
									$imagem = '../../assets/img/pets/black-cat.png'; 
								}
								
							} else {
								
								$imagem = '../../uploads/pet/' . $imagem;
							}
								
							if ($dataHora){
								$dataHora= $util->retornaData($dataHora,'br');
							}
							?>
						
							<div class='row'>
								<div class='col-4 col-sm-8'>
									
									
								<img src='<?=$imagem?>' style='width:100%'/>
								
								</div>
								<div class='col-8 col-sm-4 text-left' style='padding-top:0px; '>
								
								<p style='text-align:left'>

								<?php	
														echo "<b>" . $gestor->retornarStatus($obj->id_status)->status . "</b>, ";				
														if ($obj->cidade && $obj->estado ) {echo " " . $obj->cidade . " - " . $obj->estado . ", "; }			
														if ($obj->idade != "Não sei"){
															
																echo " " . $obj->idade . ', ';
																
															
														}								
														?>
														<?php								
														if ($obj->id_racas > 0){
															echo '#raca';
														}								
														?>
														<?php								
														if ($obj->sexo != "Não sei"){
															echo '#'. $obj->sexo;
														}								
														?>
														<?php								
														if ($obj->pedigree == "Sim"){
															echo '#pedigree';
														}								
														?>														
														<?php								
														if ($obj->castrado == "Sim"){
															if ($obj->sexo == "Fêmea"){											
																echo " #castrada";
															} else {
																echo " #castrado";
															}
														}								
														?>
														<?php								
														if ($obj->vacinado == "Sim"){
															if ($obj->sexo == "Fêmea"){										
																echo " #vacinada";
															} else {
																echo " #vacinado";
															}
															
															
														}								
														?>
														<?php								
														if ($obj->vermifugado == "Sim"){
															if ($obj->sexo == "Fêmea"){										
																echo " #vermifugada";
															} else {
																echo " #vermifugado";
															}
																
															
														}								
														?>							
								</p>
								
								<?php
								$objInterassoes = $gestor->retornarInteracoesPets(array($_SESSION['ENCONTRAPET_codigo'],$obj->id), array('id_pessoas','id_pets'));		
								//var_dump($objInterassoes);								
								$sql = "SELECT * FROM dialogos_pessoas_pets WHERE id_pets = " . $obj->id . " AND id_pessoas IN (" . $_SESSION['ENCONTRAPET_codigo'] . ", " . $obj->id_pessoas . ") ORDER BY data_hora DESC";

													
								$statement = $gestor->db->db->prepare($sql);
								//echo $sql;
								//die;
								$statement->execute();
								$objMensagens = $statement->fetchAll(PDO::FETCH_OBJ);

								?>
								


								</div>
								
								<div class='col-12'>
									

									<div style='padding-top:10px; text-align:left'>
									<hr />
									<h6>Enviar mensagem</h6>
									<form class="form" method="" action="" id='formPrincipal' name='formPrincipal'>
									<textarea class='form-control' id='mensagem' name='mensagem' style='height:100px; max-height:100px; min-height:100px; width:100%'></textarea>							
									<input type='hidden' id='operacao' name='operacao' value='mensagem' />
									<input type='hidden' id='interessado' name='interessado' value='<?=$interessado?>' />
									<input type='hidden' id='pet' name='pet' value='<?=$pet?>' />
									<button class='btn btn-primary' type='button' onclick='enviarMensagem("<?=$obj->id?>")' ><i class='fa fa-send'></i> Enviar</button>							
									</form>
									</div>
									<div style='padding-top:10px; text-align:left'>
									<hr />
									<h6>Mensagens</h6>
								</div>
							</div>		
						</div>	
					</div>	

									<?php
									if($objMensagens){
										foreach($objMensagens as $oo){
											if ($_SESSION['ENCONTRAPET_codigo'] == $oo->id_pessoas){
												$bg = "";
											} else {
												$bg = "background: rgba(255, 129, 96,0.4); ";
												

											}
											?>
					<div style='<?=$bg?>'>
						<div class='container'>
							<div class='row'>
								<div class='col-12'>											
											<div style='color:#555; text-align:left; width:100%; display:block; font-size:80%'>
												
												<p style='color:#000; padding-top:10px; padding-left:5px'><?=stripslashes($oo->texto)?></p>
												<small style='color:#555; text-align:right; width:100%; display:block'>
												<?php
												if ($_SESSION['ENCONTRAPET_codigo'] == $oo->id_pessoas){ ?>										
													<a alt='Excluir mensagem' title='Excluir mensagem' href='javascript:void(0)' onclick='excluir(<?=$oo->id?>)' style='color:rgb(233, 94, 56)'><i class='fa far fa-trash'></i></a>
												<?php
												}
												?>
												
												<b><?=$util->retornaData($oo->data_hora,'br')?> às <?=substr($oo->data_hora,10,6)?>h</b></small>
												
											</div>
											<hr />
									

									
								
															
								
								</div>	
							</div>
						</div>	
					</div>

											
											<?php	
										}					
									} else {
									?><div class='container'>
										<div class='row'>
											<div class='col-12 text-left'>
											<p style='font-size:80%; color:#000'>Nenhuma mensagem encontrada.</p>
											</div>
										</div>
									</div>	

									<?php
									}
									?>
									
									<div style='display:block; clear:left; margin-top:0px'>
										<div style='padding:5px; background:#fff; width:35px; height:35px; text-align:center; display:block; margin:0 auto; padding-top:7px' class='rounded-circle'><i class='fa fa-phone' style='color:#e95e38' > </i>
										</div>
										<div style=' padding-top:5px'>
										<small style='padding-left:10px;' >Em <?=$util->retornaData($objInterassoes->data_hora,'br')?> você informou que possui informações sobre esse Pet.</small>
										</div>
									</div>					
									
						
						
						
						<?php
						}
						?>
						
						</div>
					</div>

                    
                    
                  </div>
                  
						               
						  
                </div>







				<?php
  require("../../inc/rodape2.php");
  ?> 



                
              </div>
			
			
            
            
            </div>
         
       
                       
              
                     
              
              
           </div>
         

 
		   <?php
  require("../../inc/scripts2.php");
  ?>
  <script>
function excluir(mensagem){
	
	bootbox.hideAll()
	var dialog = bootbox.dialog({ 
		title: "Confirmação", 
		message: 'Deseja excluir esta mensagem?',
		buttons: {
			
			ok: {
			  label: "Sim",
			  className: "btn-primary",
			  callback: function() {


				$.post('process.php?operacao=excluir&msg='+mensagem,
				
				function(data){					   
					//alert(data);	
					if(data > 0){ // 1 ou maior 
									
						document.location.href = '../../procurar/interesse/?pet='+$('#pet').val()
							
									
					} else { //erro
									
						tituloMensagem = 'Erro:';
									
						tipo = 'erro';
									
						mensagem = 'Não foi possível excluir a mensagem.';
									
							
							bootbox.hideAll()
							var dialog = bootbox.dialog({ 
								title: tituloMensagem, 
								message: mensagem,
								buttons: {
									
									ok: {
									label: "Ok",
									className: "btn-primary",
									callback: function() {
										dialog.modal('hide');
									}
									}
								},					
								onEscape: function() {
									dialog.modal('hide');
								}		  
									
							});
					}
									
					
									
				});




			  }
			},
			ok2: {
			  label: "Não",
			  className: "btn-primary",
			  callback: function() {
				dialog.modal('hide');
			  }
			}
		},					
		onEscape: function() {
			dialog.modal('hide');
		}		  
			
	});		
}	  
	  function enviarMensagem(quem){
	
			var err = '';

			var quantidadeErros = 0;

			var mensagem = '';

			var tipoMensagem = '';


			tipoMensagem = 'inserido';

			tipoMensagem2 = 'inserir';

			if ($('#mensagem').val() == ''){
			
				mensagem += '- Informe alguma mensagem<br />'
				bootbox.hideAll()
							var dialog = bootbox.dialog({ 
								title: 'Erro', 
								message: mensagem,
								buttons: {
									
									ok: {
									label: "Ok",
									className: "btn-primary",
									callback: function() {
										dialog.modal('hide');
									}
									}
								},					
								onEscape: function() {
									dialog.modal('hide');
								}		  
									
							});
			} else {	 	  
			
				$.post('process.php?pet='+quem, $('#formPrincipal').serialize(),
				
				function(data){					   
					//alert(data);	
					if(data > 0){ // 1 ou maior 
									
						document.location.href = '../../procurar/interesse/?pet='+quem;		
							
									
					} else { //erro
									
						tituloMensagem = 'Erro:';
									
						tipo = 'erro';
									
						mensagem = 'Não foi possível enviar a mensagem.';
									
							
							bootbox.hideAll()
							var dialog = bootbox.dialog({ 
								title: tituloMensagem, 
								message: mensagem,
								buttons: {
									
									ok: {
									label: "Ok",
									className: "btn-primary",
									callback: function() {
										dialog.modal('hide');
									}
									}
								},					
								onEscape: function() {
									dialog.modal('hide');
								}		  
									
							});
					}
									
					
									
				});		
			}		  
		  
	  }

	$('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
	  var url = $(this).attr("href"); // the remote url for content
	  var target = $(this).data("target"); // the target pane
	  var tab = $(this); // this tab
	  
	  // ajax load from data-url
	  $(target).load(url,function(result){      
		tab.tab('show');
	  });
	});

	// initially activate the first tab..
	$('#tabIni').click();  
  </script>  
</body>

</html>
