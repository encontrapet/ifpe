<?php 
ob_start();
session_start();

/**
*	@autor Flávio Silva Brandão <flavio@afixo.com.br>
*	@version 21-01-2020 as 09:00:30
*/

/*
 * verificando se o usuario esta logado senao redireciona
 * */
if (!$_SESSION['ENCONTRAPET_codigo']){
	header("Location: ../splash");
}
# 
# REQUIRES OBRIGATORIOS
#


require('../classes/gestor.php');

#
# INSTANCIA DE OBJETOS OBRIGATORIAS
#


$util = new Util();
$gestor   = new Gestor();    

#
# TRATAMENTO DE INJECTION
#

$_POST = $util->validaParametro($_POST);
$_GET  = $util->validaParametro($_GET);
?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
  <link rel="icon" type="image/png" href="../assets/img/favicon.png">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    EncontraPet - Início
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <?php
  require("../inc/css.php");
  ?>
  
  
</head>

<body class="text-center" style=''>


              
       
              

            <div class="" style='padding:0'>      
				
				<?php
				require("../inc/navegacao.php")
				?>
            

              <!-- Nav tabs -->
              <div style="">
				  
				       
				  

                <div class="container" style='padding-left:3px; padding-right:3px; padding-top:25px; '>
                 
				 
                <div class="row">
												<div class="col-12">
													
													<h5 class="" style=''>O Encontrapet foi feito para ajudar na defesa dos animais. </h5>               
												</div>    
											</div>	
                      <div class="row">
												<div class="col-12">
                            <div class="social-line">
                                        <p>Deseja colocar um Pet para adoção?</p>
                                        <a id="btnEntrar" type='button' class="btn btn-primary" style='color: #FFFFFF;' href='../doar'  >Colocar para adoção</a>	
                                        <p>Encontrou um animal aparentemente perdido? <small>(Ex.: Possui coleira, esta bem cuidado)</small></p>
                                        <a id="btnEntrar" type='button' class="btn btn-primary" style='color: #FFFFFF;' href='#' >Relatar que Achei</a><br><br>
                                        <p>Perdeu um Pet e precisa encontra-lo?</p>
                                        <a id="btnEntrar" type='button' class="btn btn-primary" style='color: #FFFFFF;' href='#'  >Informar que Perdi</a><br><br>
                                        
                            </div>
                        </div>
                      </div>

                                            
						  
                </div>




                <?php
                require("../inc/rodape.php");
                ?>



                
              </div>
			
			
            
            
            </div>
         
       
                       
              
                     
              
              
           </div>
         

 
  <!--   Core JS Files   -->
  <?php
 require("../inc/scripts.php") ;
  ?>
  <script>

	$('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
	  var url = $(this).attr("href"); // the remote url for content
	  var target = $(this).data("target"); // the target pane
	  var tab = $(this); // this tab
	  
	  // ajax load from data-url
	  $(target).load(url,function(result){      
		tab.tab('show');
	  });
	});

	// initially activate the first tab..
	//$('#tabIni').click();  
  </script>  
</body>

</html>
