<?php 
ob_start();
session_start();
/**
*
*	@autor Flávio Silva Brandão <flavio@afixo.com.br>
*	@version 03-06-2021 as 18:06:55
*/



# 
# REQUIRES OBRIGATORIOS
#

require('../classes/gestor.php');

$util = new Util();
$gestor   = new Gestor();

#
# TRATAMENTO DE INJECTION
#

$_GET = $util->validaParametro($_GET);

extract($_GET);
extract($_POST);
# 
# VARIAVEIS  
# 

/* selecionar os dialogos e excluir */

if ($operacao == "excluir"){

	$obj = $gestor->retornarDialogosPessoasPets(array($pet,$pretendente),array('id_pets','id_pessoas'));
	$sql = "SELECT * FROM dialogos_pessoas_pets WHERE id_pets = " . $pet . " AND id_pessoas = " . $pretendente;

													
								$statement = $gestor->db->db->prepare($sql);
								//echo $sql;
								//die;
								$statement->execute();
								$obj = $statement->fetchAll(PDO::FETCH_OBJ);

	if ($obj){
		foreach($obj  as $o){
			$gestor->excluirDialogosPessoasPets($o->id);	

		}
	}
	$objInteracoesPets = $gestor->retornarInteracoesPets(array($pet,$pretendente),array('id_pets','id_pessoas'));
	$obj = $gestor->excluirInteracoesPets($objInteracoesPets->id);		
	if ($obj){		
			die('1');
	} else {
			die('0');
	}	
} elseif ($operacao == "adotar"){
//var_dump($_GET);
//die;
	$objeto = array(array('id'=>$pet, 'id_status'=>2));	
	$obj = $gestor->editarPets($objeto);
	if ($obj){		


			$objeto = array(array('id_pets'=>$pet,'id_pessoas'=>$pretendente,'data_hora'=>date('Y-m-d H:i:s')));
			$gestor->inserirInteracoesPets($objeto);	
			die('1');
	} else {
			die('0');
	}	
} 
?>
