<?php 
ob_start();
session_start();
/**
*
*	@autor Flávio Silva Brandão <flavio@afixo.com.br>
*	@version 03-06-2021 as 18:06:55
*/



# 
# REQUIRES OBRIGATORIOS
#

require('../classes/gestor.php');

$util = new Util();
$gestor   = new Gestor();

#
# TRATAMENTO DE INJECTION
#

$_GET = $util->validaParametro($_GET);

extract($_GET);
extract($_POST);
# 
# VARIAVEIS  
# 

/* selecionar os dialogos e excluir */
//var_dump($_GET);
//die;
if ($operacao == "excluir"){

	$obj = $gestor->retornarDialogosPessoasPets(array($pet,$_SESSION['ENCONTRAPET_codigo']),array('id_pets','id_pessoas'));
	if ($obj){
		foreach($obj  as $o){
			$gestor->excluirDialogosPessoasPets($o->id);	

		}
	}
	$objInteracoesPets = $gestor->retornarInteracoesPets(array($pet,$_SESSION['ENCONTRAPET_codigo']),array('id_pets','id_pessoas'));
	$obj = $gestor->excluirInteracoesPets($objInteracoesPets->id);		
	if ($obj){		
			die('1');
	} else {
			die('0');
	}	
} 
?>
