<?php
ini_set('display_errors', 0);
ini_set('display_startup_errors', 0);
//error_reporting(E_ALL && ~E_NOTICE);

/**
*	@autor Flávio Silva Brandão <flavio@afixo.com.br>
*	@created 21-01-2020 as 10:16:17
*	@version 06-02-2020 as 16:26:30
*/

if (file_exists('classes/db.php')){
	
	require('classes/db.php');
	require('classes/util/util.php');
	require("classes/models/PessoasEnderecos.php"); 
	require("classes/models/Especies.php"); 
	require("classes/models/Fotos.php"); 
	require("classes/models/Pessoas.php"); 
	require("classes/models/Pets.php"); 
	require("classes/models/Status.php"); 
	require("classes/models/PessoasTipos.php"); 


	require("classes/models/Racas.php"); 


	require("classes/models/InteracoesPets.php"); 
	require("classes/models/DialogosPessoasPets.php"); 
	require("classes/models/MovimentacoesPets.php"); 

} elseif (file_exists('../classes/db.php')){
		
	require('../classes/db.php');
	require('../classes/util/util.php');
	require("../classes/models/PessoasEnderecos.php"); 
	require("../classes/models/Especies.php"); 
	require("../classes/models/Fotos.php"); 
	require("../classes/models/Pessoas.php"); 
	require("../classes/models/Pets.php"); 
	require("../classes/models/Status.php"); 
	require("../classes/models/PessoasTipos.php"); 


	require("../classes/models/Racas.php"); 


	require("../classes/models/InteracoesPets.php"); 
	require("../classes/models/DialogosPessoasPets.php"); 
	require("../classes/models/MovimentacoesPets.php"); 

} else {
	
	require('../../classes/db.php');
	require('../../classes/util/util.php');
	require("../../classes/models/PessoasEnderecos.php"); 
	require("../../classes/models/Especies.php"); 
	require("../../classes/models/Fotos.php"); 
	require("../../classes/models/Pessoas.php"); 
	require("../../classes/models/Pets.php"); 
	require("../../classes/models/Status.php"); 
	require("../../classes/models/PessoasTipos.php"); 


	require("../../classes/models/Racas.php"); 	

	require("../../classes/models/InteracoesPets.php"); 
	require("../../classes/models/DialogosPessoasPets.php"); 
	require("../../classes/models/MovimentacoesPets.php");

} 

class Gestor {

	public $db;
	private $pessoasEnderecos;
	private $especies;
	private $fotos;
	private $pessoas;
	private $pets;
	private $status;
	private $pessoasTipos;
	private $racas;
	
	private $interacoesPets;
	private $dialogosPessoasPets;
	private $movimentacoesPets;
	
	
	public function __construct() {
		
		
		$this->especies 		= new Especies();
		$this->fotos 			= new Fotos();
		$this->pessoas 			= new Pessoas();
		$this->pessoasEnderecos	= new PessoasEnderecos();
		$this->pets 			= new Pets();
		$this->status 			= new Status();
		$this->pessoasTipos 	= new PessoasTipos();
		$this->racas			= new Racas();
		
		$this->interacoesPets	= new InteracoesPets();
		$this->dialogosPessoasPets = new DialogosPessoasPets();
		$this->movimentacoesPets = new MovimentacoesPets();
		

			$this->db = new DB(array('metodo'=>1));
			$this->db->conectar();
					
		}

		// SETOR REFERENTE AOS METODOS GENERICOS

		public function montaOptionsSelect($tabela, $nomeExibicao, $selecionado, $ordenacao = '', $ordenacao_tipo = '',  $remover = ''){	

			$this->db->montaOptionsSelect($tabela, $nomeExibicao, $selecionado, $ordenacao = '', $ordenacao_tipo = '',  $remover = '');

		}


	// SETOR REFERENTE AS CLASSES DA TABELA Pessoas Enderecos

	public function retornarPessoasEnderecos($id, $campo = 'id', $tabela = 'pessoas_enderecos', $ordenadoPor = '', $order = '', $limit = '') {
		$retorno = $this->pessoasEnderecos->retornar($id, $campo , $tabela, $ordenadoPor, $order, $limit);			
		if($retorno) {
			return $retorno;
		} else {
			return false;
		}
	}
	public function inserirPessoasEnderecos($objeto) {
	
		 $retorno = $this->pessoasEnderecos->inserir($objeto);
				 
		 if($retorno) {
		 	return $retorno;
		 } else {
		 	return false;
		 }
	}
	public function editarPessoasEnderecos($objeto,$campo = 'id') {
		$retorno = $this->pessoasEnderecos->editar($objeto,$campo);
				 
		if($retorno) {
			return true;
		} else {
			return false;
		}
	}
	public function excluirPessoasEnderecos($valor,$campo = 'id') {
		$retorno = $this->pessoasEnderecos->excluir($valor,$campo) ;

				 
		if($retorno) {
				return true;
		} else {
			return false;
		}
	}


	// SETOR REFERENTE AS CLASSES DA TABELA ESPECIES

	public function retornarEspecies($id, $campo = 'id', $tabela = 'especies', $ordenadoPor = '', $order = '', $limit = '') {
		$retorno = $this->especies->retornar($id, $campo , $tabela, $ordenadoPor, $order, $limit);
					
		if($retorno) {
			return $retorno;
		} else {
			return false;
		}
	}
	public function inserirEspecies($objeto) {
		$retorno = $this->especies->inserir($objeto);
				 
		if($retorno) {
			return $retorno;
		} else {
			return false;
		}
	}
	public function editarEspecies($objeto,$campo = 'id') {
		$retorno = $this->especies->editar($objeto,$campo);
				 
		if($retorno) {
			return true;
		} else {
			return false;
		}
	}
	public function excluirEspecies($valor,$campo = 'id') {
		$retorno = $this->especies->excluir($valor,$campo) ;

				 
		if($retorno) {
				return true;
		} else {
			return false;
		}
	}


	// SETOR REFERENTE AS CLASSES DA TABELA FOTOS

	public function retornarFotos($id, $campo = 'id', $tabela = 'fotos', $ordenadoPor = '', $order = '', $limit = '') {
		$retorno = $this->fotos->retornar($id, $campo , $tabela, $ordenadoPor, $order, $limit);
					
		if($retorno) {
			return $retorno;
		} else {
			return false;
		}
	}
	public function inserirFotos($objeto) {
		$retorno = $this->fotos->inserir($objeto);
				 
		if($retorno) {
			return $retorno;
		} else {
			return false;
		}
	}
	public function editarFotos($objeto,$campo = 'id') {
		$retorno = $this->fotos->editar($objeto,$campo);
				 
		if($retorno) {
			return true;
		} else {
			return false;
		}
	}
	public function excluirFotos($valor,$campo = 'id') {
		$retorno = $this->fotos->excluir($valor,$campo) ;

				 
		if($retorno) {
				return true;
		} else {
			return false;
		}
	}


	// SETOR REFERENTE AS CLASSES DA TABELA PESSOAS

	public function retornarPessoas($id, $campo = 'id', $tabela = 'pessoas', $ordenadoPor = '', $order = '', $limit = '') {
		$retorno = $this->pessoas->retornar($id, $campo , $tabela, $ordenadoPor, $order, $limit);
					
		if($retorno) {
			return $retorno;
		} else {
			return false;
		}
	}
	public function inserirPessoas($objeto) {
		$retorno = $this->pessoas->inserir($objeto);
				 
		if($retorno) {
			return $retorno;
		} else {
			return false;
		}
	}
	public function editarPessoas($objeto,$campo = 'id') {
		$retorno = $this->pessoas->editar($objeto,$campo);
				 
		if($retorno) {
			return true;
		} else {
			return false;
		}
	}
	public function excluirPessoas($valor,$campo = 'id') {
		$retorno = $this->pessoas->excluir($valor,$campo) ;

				 
		if($retorno) {
				return true;
		} else {
			return false;
		}
	}


	// SETOR REFERENTE AS CLASSES DA TABELA PETS

	public function retornarPets($id, $campo = 'id', $tabela = 'pets', $ordenadoPor = '', $order = '', $limit = '') {
		$retorno = $this->pets->retornar($id, $campo , $tabela, $ordenadoPor, $order, $limit);
					
		if($retorno) {
			return $retorno;
		} else {
			return false;
		}
	}
	public function inserirPets($objeto) {
		
		$retorno = $this->pets->inserir($objeto);
				 
		if($retorno) {
			return $retorno;
		} else {
			return false;
		}
	}
	public function editarPets($objeto,$campo = 'id') {
		$retorno = $this->pets->editar($objeto,$campo);
				 
		if($retorno) {
			return true;
		} else {
			return false;
		}
	}
	public function excluirPets($valor,$campo = 'id') {
		$retorno = $this->pets->excluir($valor,$campo) ;

				 
		if($retorno) {
				return true;
		} else {
			return false;
		}
	}


	// SETOR REFERENTE AS CLASSES DA TABELA STATUS

	public function retornarStatus($id, $campo = 'id', $tabela = 'status', $ordenadoPor = '', $order = '', $limit = '') {
		$retorno = $this->status->retornar($id, $campo , $tabela, $ordenadoPor, $order, $limit);
					
		if($retorno) {
			return $retorno;
		} else {
			return false;
		}
	}
	public function inserirStatus($objeto) {
		$retorno = $this->status->inserir($objeto);
				 
		if($retorno) {
			return $retorno;
		} else {
			return false;
		}
	}
	public function editarStatus($objeto,$campo = 'id') {
		$retorno = $this->status->editar($objeto,$campo);
				 
		if($retorno) {
			return true;
		} else {
			return false;
		}
	}
	public function excluirStatus($valor,$campo = 'id') {
		$retorno = $this->status->excluir($valor,$campo) ;

				 
		if($retorno) {
				return true;
		} else {
			return false;
		}
	}


	// SETOR REFERENTE AS CLASSES DA TABELA pessoas TIPOS

	public function retornarPessoasTipos($id, $campo = 'id', $tabela = 'pessoas_tipos', $ordenadoPor = '', $order = '', $limit = '') {
		$retorno = $this->pessoasTipos->retornar($id, $campo , $tabela, $ordenadoPor, $order, $limit);
					
		if($retorno) {
			return $retorno;
		} else {
			return false;
		}
	}
	public function inserirPessoasTipos($objeto) {
		$retorno = $this->pessoasTipos->inserir($objeto);
				 
		if($retorno) {
			return $retorno;
		} else {
			return false;
		}
	}
	public function editarPessoasTipos($objeto,$campo = 'id') {
		$retorno = $this->pessoasTipos->editar($objeto,$campo);
				 
		if($retorno) {
			return true;
		} else {
			return false;
		}
	}
	public function excluirPessoasTipos($valor,$campo = 'id') {
		$retorno = $this->pessoasTipos->excluir($valor,$campo) ;

				 
		if($retorno) {
				return true;
		} else {
			return false;
		}
	}



	// SETOR REFERENTE AS CLASSES DA TABELA racas

	public function retornarRacas($id, $campo = 'id', $tabela = 'racas', $ordenadoPor = '', $order = '', $limit = '') {
		$retorno = $this->racas->retornar($id, $campo , $tabela, $ordenadoPor, $order, $limit);
					
		if($retorno) {
			return $retorno;
		} else {
			return false;
		}
	}
	public function inserirRacas($objeto) {
		$retorno = $this->racas->inserir($objeto);
				 
		if($retorno) {
			return $retorno;
		} else {
			return false;
		}
	}
	public function editarRacas($objeto,$campo = 'id') {
		$retorno = $this->racas->editar($objeto,$campo);
				 
		if($retorno) {
			return true;
		} else {
			return false;
		}
	}
	public function excluirRacas($valor,$campo = 'id') {
		$retorno = $this->racas->excluir($valor,$campo) ;

				 
		if($retorno) {
				return true;
		} else {
			return false;
		}
	}


	// SETOR REFERENTE AS CLASSES DA TABELA InteracoesPets

	public function retornarInteracoesPets($id, $campo = 'id', $tabela = 'interacoes_pets', $ordenadoPor = '', $order = '', $limit = '') {
		$retorno = $this->interacoesPets->retornar($id, $campo , $tabela, $ordenadoPor, $order, $limit);
					
		if($retorno) {
			return $retorno;
		} else {
			return false;
		}
	}
	public function inserirInteracoesPets($objeto) {
		$retorno = $this->interacoesPets->inserir($objeto);
				 
		if($retorno) {
			return $retorno;
		} else {
			return false;
		}
	}
	public function editarInteracoesPets($objeto,$campo = 'id') {
		$retorno = $this->interacoesPets->editar($objeto,$campo);
				 
		if($retorno) {
			return true;
		} else {
			return false;
		}
	}
	public function excluirInteracoesPets($valor,$campo = 'id') {
		$retorno = $this->interacoesPets->excluir($valor,$campo) ;

				 
		if($retorno) {
				return true;
		} else {
			return false;
		}
	}


	// SETOR REFERENTE AS CLASSES DA TABELA DialogosPessoasPets

	public function retornarDialogosPessoasPets($id, $campo = 'id', $tabela = 'dialogos_pessoas_pets', $ordenadoPor = '', $order = '', $limit = '') {
		$retorno = $this->dialogosPessoasPets->retornar($id, $campo , $tabela, $ordenadoPor, $order, $limit);
					
		if($retorno) {
			return $retorno;
		} else {
			return false;
		}
	}
	public function inserirDialogosPessoasPets($objeto) {
		$retorno = $this->dialogosPessoasPets->inserir($objeto);
				 
		if($retorno) {
			return $retorno;
		} else {
			return false;
		}
	}
	public function editarDialogosPessoasPets($objeto,$campo = 'id') {
		$retorno = $this->dialogosPessoasPets->editar($objeto,$campo);
				 
		if($retorno) {
			return true;
		} else {
			return false;
		}
	}
	public function excluirDialogosPessoasPets($valor,$campo = 'id') {
		$retorno = $this->dialogosPessoasPets->excluir($valor,$campo) ;

				 
		if($retorno) {
				return true;
		} else {
			return false;
		}
	}


	// SETOR REFERENTE AS CLASSES DA TABELA MovimentacoesPets

	public function retornarMovimentacoesPets($id, $campo = 'id', $tabela = 'movimentacoes_pets', $ordenadoPor = '', $order = '', $limit = '') {
		$retorno = $this->movimentacoesPets->retornar($id, $campo , $tabela, $ordenadoPor, $order, $limit);
					
		if($retorno) {
			return $retorno;
		} else {
			return false;
		}
	}
	public function inserirMovimentacoesPets($objeto) {
		$retorno = $this->movimentacoesPets->inserir($objeto);
				 
		if($retorno) {
			return $retorno;
		} else {
			return false;
		}
	}
	public function editarMovimentacoesPets($objeto,$campo = 'id') {
		$retorno = $this->movimentacoesPets->editar($objeto,$campo);
				 
		if($retorno) {
			return true;
		} else {
			return false;
		}
	}
	public function excluirMovimentacoesPets($valor,$campo = 'id') {
		$retorno = $this->movimentacoesPets->excluir($valor,$campo) ;

				 
		if($retorno) {
				return true;
		} else {
			return false;
		}
	}
}
?>
