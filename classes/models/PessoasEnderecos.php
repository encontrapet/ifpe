<?php

/**
*	@Desenvolvido por Afixo Agência WEB
*	@url www.afixo.com.br
*	@email gestor@afixo.com.br
*
*	@autor Flávio Silva Brandão <flavio@afixo.com.br>
*	@version 12-05-2021 as 07:39:30
*/

class PessoasEnderecos extends DB {

	private $tabela = 'pessoas_enderecos';
	
	private $id_pessoas;

	private $cep;
	private $bairro;
	private $endereco;
	private $numero;
	private $complemento;
	private $cidade;
	private $uf;

	
	public function __construct($id_pessoas="",$cep="",$bairro="",$endereco="",$numero="",$complemento="",$uf="",$cidade="") {
		
		$this->id_pessoas = $id_pessoas;
		$this->cep = $cep;
		$this->bairro = $bairro;
		$this->endereco = $endereco;
		$this->numero = $numero;
		$this->complemento = $complemento;
		$this->uf = $uf;
		$this->cidade = $cidade;
	}

	public function retornar($id, $campo = 'id', $tabela = 'pessoas_enderecos', $ordenadoPor = '', $order = '', $limit = '') {
		return parent::retornar($id, $campo, $tabela, $ordenadoPor, $order, $limit);
	}

	public function inserir($objeto, $tabela = 'pessoas_enderecos') {
		return parent::inserir($objeto, $tabela);
	}

	public function editar($objeto, $campo = 'id', $tabela = 'pessoas_enderecos') {
		return parent::editar($objeto, $campo, $tabela);
	}

	public function excluir($valor, $campo = 'id', $tabela = 'pessoas_enderecos'){
		return parent::excluir($valor, $campo, $tabela);
	}

}
?>
