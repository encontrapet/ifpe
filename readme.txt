Instruções gerais:
- instalar apache2 + php 7.2 + mysql 5.7
- criar a base de dados encontrapet
- É Necessário para o funcionamento instalar a base de dados "encontrapet.sql"
- No arquivo classes/db.php -> alterar a linha 18 para usuario e senha configurado em seu mysql
- Dar permissão de escrita na pasta uploads
- Apagar algum arquivo que esteja nas pastas internas
----x----
Para os testes Unitários:
Testes encontram-se no diretório testes/testUnidades.php
Duplicar o banco de dados utilizando como padrão "nome_utilizado_sandbox"
Ajustar as configurações de banco de dados no arquivo /classes/db.php
----x----
Para testes de Sistema com Selenium:
-Instalar plugin Web no Navegador desejado (Chrome ou Mozilla)
- Carregar o projeto contido no arquivo 'tests/EncontraPet.side'
-Criar pasta denominada 'fakepath' no diretório C:, e carregar inserir alguma foto a ser usada pelos testes,
- Acessar as configurações do Plugin e habilitar a opção "Permitir acesso a URLs de arquivo"
-Ao realizar testes consecutivos, atenção para os valores de email utilizados, pois são estáticos, o ideal é que a cada
execução os usuários teste criados sejam eliminados do banco para não ocorrer erros de duplicidade.
----x----
Para testes Serviços (Api):
Testes encontram-se no diretório testes/TestesServicos.php
Utilizar PHP unit versão 9.4
Utilizar em uma base de dados duplicada especifica para testes
Suites são executadas com o comando: 'phpunit'


