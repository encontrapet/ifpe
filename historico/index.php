<?php 
/**
 * 
 * 
*	@autor Flávio Silva Brandão <flavio@afixo.com.br>
*	@version 03-06-2021 as 09:00:30
*
* 
* */



/*
 * habilitando o uso de sessoes
 * */

ob_start();
session_start();

/*
 * verificando se o usuario esta logado senao redireciona
 * */
if (!$_SESSION['ENCONTRAPET_codigo']){
	header("Location: ../splash");
}

# 
# REQUIRES OBRIGATORIOS
#


require('../classes/gestor.php');

#
# INSTANCIA DE OBJETOS OBRIGATORIAS
#


$util = new Util();
$gestor   = new Gestor();    

#
# TRATAMENTO DE INJECTION
#

$_POST = $util->validaParametro($_POST);
$_GET  = $util->validaParametro($_GET);


/*
 * 
 * reuperar os pets cadastrados anteriormente 1,3,6 */
 
$sql = "SELECT DISTINCT id_pets FROM interacoes_pets WHERE id_pets NOT IN(SELECT id FROM pets WHERE  id_pessoas = " . $_SESSION['ENCONTRAPET_codigo'] . ") AND id_pessoas = " . $_SESSION['ENCONTRAPET_codigo'] . "  ";

													
$statement = $gestor->db->db->prepare($sql);
//echo $sql;
//die;
$statement->execute();
$objetoInterassoesPets = $statement->fetchAll(PDO::FETCH_OBJ);


//var_dump($objetoPets);
//die;
?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
  <link rel="icon" type="image/png" href="../assets/img/favicon.png">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    EncontraPet - Início
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <?php
  require("../inc/css.php");
  ?>
</head>

<body class="text-center" style=''>


              
       
              

            <div class="" style='padding:0'>      
				
				<?php
				require("../inc/navegacao.php")
				?>
            

              <!-- Nav tabs -->
              <div style="">
				  
				       
				  

                <div class="" style='padding-left:3px; padding-right:3px; padding-top:5px; '>
                  <!-- Tab panes -->
                  <div class="tab-content text-left">

					  
                    <div class="tab-pane active" id="pets" role="tabpanel">
						<div class='container-fluid' style='padding-bottom:20px; padding-top:20px'>
							<div class="row">
								<div class="col-12">
									
									<h5 class="" style=''>Histórico</h5>               
								</div>    
							</div>	
							
						<?php
						if ($objetoInterassoesPets){
							?>
							<div class='card-columns' style='padding-bottom:0; margin-bottom:0'>
							<?php
							foreach($objetoInterassoesPets as $oo){

								$obj =  $gestor->retornarPets($oo->id_pets);
								
								$dataHora = $gestor->retornarMovimentacoesPets(array($obj->id,$obj->id_status),array('id_pets','id_status'),'movimentacoes_pets','id','DESC',1)->data_hora;
								$imagem = $gestor->retornarFotos($obj->id,'id_pets','fotos','id','DESC',1)->caminho;
								
								
								
								$objetoInteracoesPets = $gestor->retornarInteracoesPets($obj->id,'id_pets','interacoes_pets','id','DESC');
								if ($objetoInteracoesPets){
									if (@count($objetoInteracoesPets) < 2){
										$objetoInteracoesPets = array($objetoInteracoesPets);
									}
								}
																
								$sql = "SELECT id_pessoas FROM dialogos_pessoas_pets WHERE id_pets = " . $obj->id . " AND id_pessoas <> " . $_SESSION['ENCONTRAPET_codigo'] . " GROUP BY id_pessoas";

													
								$statement = $gestor->db->db->prepare($sql);
								//echo $sql;
								//die;
								$statement->execute();
								$objetoConversasPets = $statement->fetchAll(PDO::FETCH_OBJ);

								if (!$imagem){
									if ($obj->id_especies == 1){ // cachorros
										$imagem = '../assets/img/pets/black-dog.png'; 
									} else {
										$imagem = '../assets/img/pets/black-cat.png'; 
									}
									
								} else {
									
									$imagem = '../uploads/pet/' . $imagem;
								}
									
								if ($dataHora){
									$dataHora= $util->retornaData($dataHora,'br');
								}
								?>
							
								<div class='card'  style='padding-bottom:5px;  padding-top:15px;  margin-bottom:10px;  background:rgba(233, 94, 56,0.4); border:0; box-shadow:none'>	
									
										
										
									<img src='<?=$imagem?>' style='width:94%; display:block; margin:0 auto; margin-bottom:10px'/>
									<div class='container' style=''>
										<div class='row' style='padding-bottom:10px'>
											<div class='col-12'>
												<small style='float:right'>Cadastrada em: 
												<?php								
												
													echo $dataHora;
																				
												?>
												</small>
												<b><?=$gestor->retornarStatus($obj->id_status)->status?></b><br />
												<?php								
												if ($obj->sexo != "Não sei"){
													echo $obj->sexo;
												}								
												?>
											</div>
										</div>
									</div>
									
									
									<div class='container' style=''>
										<div class='row' style=''>
											
											
											<div class='col-3' style='padding:0; background:rgb(233, 94, 56)'>
												
											</div>
											<div class='col-2' style='padding-right:0; background:rgb(233, 94, 56)'>
												<a href='javascript:void(0)' class='btn btn-primary col-2' style='width:100%; padding-left:10px ' title='Excluir interesse' alt='Excluir interesse' onclick='excluir(<?=$obj->id?>)'><i class='fa fa-trash'></i></a>									
											
											</div>
											<div class='col' style='margin:0; background:rgb(233, 94, 56)'>
												<?php
												
												if ($obj->id_status < 3) {

													$pastinha = "adotar";
												} else {

													$pastinha = "procurar";
												}
												?>

												<a class="btn btn-primary "  style='width:100%; padding-left:30px; padding-right:30px' data-toggle="" href="../<?=$pastinha?>/interesse/?pet=<?=$obj->id?>" role="button" >
													<i class="far fa-comments"></i>
													<?php
													
													
														echo "Conversas";
													
													?>	
												</a>
											</div>
											
										</div>	
									</div>	
										


									</div>

							
							
							
							<?php
							}
							?>
							</div>
						<?php
						} else {
						?>
						<p>Nenhum pet cadastrado até o momento.</p>
						<?php						
						}
						?>
						
						</div>
					</div>

                   
                    
                  </div>
                  
						               
						  
                </div>




				<?php
				require("../inc/rodape.php");
				?>  



                
              </div>
			
			
            
            
            </div>
         
       
                       
              
                     
              
              
           </div>
         

 
  <!--   Core JS Files   -->
  <?php
 require("../inc/scripts.php") ;
  ?>
  <script>
	function excluir(pet){
	
	bootbox.hideAll()
	var dialog = bootbox.dialog({ 
		title: "Confirmação", 
		message: 'Deseja excluir este Pet da sua lista?',
		buttons: {
			
			ok: {
			  label: "Sim",
			  className: "btn-primary",
			  callback: function() {


				$.post('process.php?operacao=excluir&pet='+pet,
				
				function(data){					   
					alert(data);	
					if(data > 0){ // 1 ou maior 
									
						document.location.href = '../historico'							
									
					} else { //erro
									
						tituloMensagem = 'Erro:';
									
						tipo = 'erro';
									
						mensagem = 'Não foi possível excluir a mensagem.';
									
							
							bootbox.hideAll()
							var dialog = bootbox.dialog({ 
								title: tituloMensagem, 
								message: mensagem,
								buttons: {
									
									ok: {
									label: "Ok",
									className: "btn-primary",
									callback: function() {
										dialog.modal('hide');
									}
									}
								},					
								onEscape: function() {
									dialog.modal('hide');
								}		  
									
							});
					}
									
					
									
				});




			  }
			},
			ok2: {
			  label: "Não",
			  className: "btn-primary",
			  callback: function() {
				dialog.modal('hide');
			  }
			}
		},					
		onEscape: function() {
			dialog.modal('hide');
		}		  
			
	});		
}

	$('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
	  var url = $(this).attr("href"); // the remote url for content
	  var target = $(this).data("target"); // the target pane
	  var tab = $(this); // this tab
	  
	  // ajax load from data-url
	  $(target).load(url,function(result){      
		tab.tab('show');
	  });
	});

	// initially activate the first tab..
	$('#tabIni').click();  
  </script>  
</body>

</html>
