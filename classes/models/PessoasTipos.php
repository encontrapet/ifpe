<?php

/**
*	@autor Flávio Silva Brandão <flavio@afixo.com.br>
*	@version 12-05-2021 as 07:38:17
*/

class PessoasTipos extends DB {

	private $tabela = 'pessoas_tipos';
	public $id;
	public $tipo;

	/**
	* Método construtor do objeto tipos  
	 @param inteiro id
	 @param string tipo

	**/
	public function __construct($id="",$tipo="") {
		$this->id = $id;
		$this->tipo = $tipo;
	}

	public function retornar($id, $campo = 'id', $tabela = 'pessoas_tipos', $ordenadoPor = '', $order = '', $limit = '') {
		return parent::retornar($id, $campo, $tabela, $ordenadoPor, $order, $limit);
	}

	public function inserir($objeto, $tabela = 'pessoas_tipos') {
		return parent::inserir($objeto, $tabela);
	}

	public function editar($objeto, $campo = 'id', $tabela = 'pessoas_tipos') {
		return parent::editar($objeto, $campo, $tabela);
	}

	public function excluir($valor, $campo = 'id', $tabela = 'pessoas_tipos'){
		return parent::excluir($valor, $campo, $tabela);
	}

}
?>