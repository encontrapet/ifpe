<?php
/**
 * 
 * 
*	@autor Flávio Silva Brandão <flavio@afixo.com.br>
*	@version 26-05-2021 as 09:00:30
*
* 
* */



/*
 * habilitando o uso de sessoes
 * */

ob_start();
session_start();

/*
 * verificando se o usuario esta logado senao redireciona
 * */
if (!$_SESSION['ENCONTRAPET_codigo']){
	header("Location: ../splash");
}

# 
# REQUIRES OBRIGATORIOS
#


require('../classes/gestor.php');

#
# INSTANCIA DE OBJETOS OBRIGATORIAS
#


$util = new Util();
$gestor   = new Gestor();    

#
/*
 * 
 * reuperar os pets cadastrados anteriormente */
if ($_POST){ 
	extract($_POST);
	//var_dump($_POST);
	//die;

	if ($uf){
		$url = "https://servicodados.ibge.gov.br/api/v1/localidades/estados/" . $uf;
        $data = json_decode(file_get_contents($url), true);
        $estado = $data['sigla'];

		$estadoSql = " AND estado = '" . $estado . "'";
		
	}
	if ($id_especies){		
		$especieSql = " AND id_especies = '" . $id_especies . "'";		
	}
	if ($castrado){		
		$castradoSql = " AND castrado = '" . ($castrado) . "'";		
	}
	if ($vermifugado){		
		$vermifugadoSql = " AND vermifugado = '" . ($vermifugado) . "'";		
	}
	if ($vacinado){		
		$vacinadoSql = " AND vacinado = '" . ($vacinado) . "'";		
	}
	if ($pedigree){		
		$pedigreeSql = " AND pedigree = '" . ($pedigree) . "'";		
	}
	if ($id_especies){		
		$especieSql = " AND id_especies = '" . $id_especies . "'";		
	}
	if ($sexo){		
		$sexoSql = " AND sexo = '" . ($sexo) . "'";		
	}
	//die;

	$objetoPetsAdocao = $gestor->retornarPets(1,'id_status','pets','id','DESC');

	$sql = "SELECT * FROM pets WHERE id_status = 1 $vacinadoSql $pedigreeSql $especieSql $castradoSql $vermifugadoSql $sexoSql $estadoSql AND id_pessoas <> " . $_SESSION['ENCONTRAPET_codigo'] . "   ORDER BY id DESC";

														
	$statement = $gestor->db->db->prepare($sql);
	//echo $sql;
	//die;
	$statement->execute();
	$objetoPetsAdocao = $statement->fetchAll(PDO::FETCH_OBJ);
} else {
	$objetoPetsAdocao = $gestor->retornarPets(1,'id_status','pets','id','DESC');

	$sql = "SELECT * FROM pets WHERE id_status = 1  AND id_pessoas <> " . $_SESSION['ENCONTRAPET_codigo'] . "   ORDER BY id DESC";

														
	$statement = $gestor->db->db->prepare($sql);
	//echo $sql;
	//die;
	$statement->execute();
	$objetoPetsAdocao = $statement->fetchAll(PDO::FETCH_OBJ);
}
//var_dump($objetoPetsAdocao);
//die;
?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
  <link rel="icon" type="image/png" href="../assets/img/favicon.png">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    EncontraPet - Início
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <?php
  require("../inc/css.php");
  ?>
  
  
</head>

<body class="text-center" style=''>


              
       
              

            <div class="" style='padding:0; padding-bottom:80px'>      
				
				<?php
				require("../inc/navegacao.php")
				?>
            

              
					<div class='container-fluid text-left'>
								<div class='row' style='padding-bottom:20px; padding-top:20px'>
									<div class='col-12 text-right'>
									
										<a class="card-title" data-toggle="collapse" href="#divFiltros" role="button" aria-expanded="false" aria-controls="divFiltros" style='color:#333; text-decoration:none'><i class="fa fa-search"></i> Filtros </a>
										
									</div>
								</div>
								<div class="collapse" id="divFiltros">	

									<form id='frmFiltro' method="POST" name='frmFiltro' enctype="multipart/form-data">
									<div class='row' style='padding-bottom:20px'>					
										<div class='col-6 col-sm-3' style='padding-bottom:5px; '>	
											<label>Espécie:</label>
											<select class='form-control' id='id_especies' name='id_especies'>
												<option <?php if ($id_especies == ""){ echo "selected";} ?> value=''>Todos</option>
												<option <?php if ($id_especies == "1"){ echo "selected";} ?> value='1'>Cães</option>
												<option <?php if ($id_especies == "2"){ echo "selected";} ?> value='2'>Gatxs</option>
												
											</select>
										</div>
										<div class='col-6 col-sm-3' style='padding-bottom:5px; '>	
											<label>Sexo:</label>
											<select class='form-control' name='sexo' id='sexo'>
												<option <?php if ($sexo == ""){ echo "selected";} ?> value='' >Ambos</option>
												<option <?php if ($sexo == "Fêmea"){ echo "selected";} ?> value='Fêmea'>Fêmea</option>
												<option <?php if ($sexo == "Macho"){ echo "selected";} ?> value='Macho'>Macho</option>
											</select>
										</div>
										<div class='col-6 col-sm-3' style='padding-bottom:5px; '>	
											<label style='display:block'>Castrado:</label>
											<select class='form-control' name='castrado' id='castrado'>
												<option <?php if ($castrado == ""){ echo "selected";} ?> value=''>Não importa</option>
												<option <?php if ($castrado == "Sim"){ echo "selected";} ?> value='Sim'>Sim</option>
												<option <?php if ($castrado == "Não"){ echo "selected";} ?> value='Não'>Não</option>
											</select>
											
											
										</div>
										<div class='col-6 col-sm-3' style='padding-bottom:5px; '>	
											<label style='display:block'>Vermifugado:</label>
											<select class='form-control' name='vermifugado' id='vermifugado'>
												<option <?php if ($vermigugado == ""){ echo "selected";} ?> value=''>Não importa</option>
												<option <?php if ($vermigugado == "Sim"){ echo "selected";} ?> value='Sim'>Sim</option>
												<option <?php if ($vermigugado == "Não"){ echo "selected";} ?> value='Não'>Não</option>
											</select>
											
											
										</div>
										<div class='col-6 col-sm-3' style='padding-bottom:5px; '>	
											<label style='display:block'>Pedigree:</label>
											<select class='form-control' name='pedigree' id='pedigree'>
												<option <?php if ($pedigree == ""){ echo "selected";} ?> value=''>Não importa</option>
												<option <?php if ($pedigree == "Sim"){ echo "selected";} ?> value='Sim'>Sim</option>
												<option <?php if ($pedigree == "Não"){ echo "selected";} ?> value='Não'>Não</option>
											</select>
											
											
										</div>
										<div class='col-6 col-sm-3' style='padding-bottom:5px; '>	
											<label style='display:block'>Vacinado:</label>
											<select class='form-control' name='vacinado' id='vacinado'>
												<option <?php if ($vacinado == ""){ echo "selected";} ?> value=''>Não importa</option>
												<option <?php if ($vacinado == "Sim"){ echo "selected";} ?> value='Sim'>Sim</option>
												<option <?php if ($vacinado == "Não"){ echo "selected";} ?> value='Não'>Não</option>
											</select>
											
											
										</div>


										
										<div class='col-6 col-sm-3' style='padding-bottom:5px; '>	
											<label>Estado:</label>


											
											<select class='form-control' name='uf' id='uf' onchange='cidades()'>
												<option value=''>Todos</option>
												<?php
												$url = "https://servicodados.ibge.gov.br/api/v1/localidades/estados?orderBy=nome";
												$data = json_decode(file_get_contents($url), true);
												foreach($data as $d){
													if ($d['id'] == $uf) {

														$selecionado = "selected";
													} else {
														$selecionado = "";
													}
													echo("<option value='" . $d['id'] . "' $selecionado >" . $d['nome'] . "</option>");
												}
												?>
											</select>
										</div>
										<div class='col-6 col-sm-3' style='padding-bottom:5px; ' id='divCidades'>	
											<label>Cidade:</label>
											<select class='form-control' name='cidade' id='cidade' >
												<option <?php if ($cidade == ""){ echo "selected";} ?> >Todos</option>
											</select>
										</div>
										
										<div class='col-6 col-sm-3' style='padding-bottom:5px; padding-top:20px; '>	
									
											
											<button class="btn btn-primary btn-round btn-lg" style='width:100%' type='button' onclick='filtrar()'><i class="fa fa-search"></i> Filtrar</button>
										</div>
									</div>
						
									</form>			
								</div>
								
									
						<div class='card-columns' style='padding-bottom:0; margin-bottom:0'>
								
											<?php
											foreach($objetoPetsAdocao as $obj){
												
												//if ( $obj->id_pessoas != $_SESSION['ENCONTRAPET_codigo']){ // se o pet nao for meu
													
													$imagem = $gestor->retornarPets($obj->id,'id_pets','fotos','id','DESC',1)->caminho;
													
													if (!$imagem){
														if ($obj->id_especies == 1){ // cachorros
															$imagem = '../assets/img/pets/black-dog.png'; 
														} else {
															$imagem = '../assets/img/pets/black-cat.png'; 
														}
														
													} else {
														
														$imagem = '../uploads/pet/' . $imagem;
													}
														
													if (@$obj->data_cadastro){
														$dataHora= $util->retornaData($obj->data_cadastro,'br');
													}
												?>	
										
													<div class='card'  style='padding-bottom:0;  margin-bottom:0; background:transparent; border:0; box-shadow:none'>	
														
														<figure width='100%' style='box-shadow:none; padding-bottom:0; '>
														<a href="../adotar/pet/?pet=<?=$obj->id?>">	
														
														<img class="d-block" src="<?=$imagem?>" alt="Dog 2" width='100%'>
														<figcaption><h6 style='padding-bottom:0; margin-top:0; margin-bottom:0'><?=$obj->nome?></h6>
														<?php					
														if ($obj->cidade && $obj->estado ) {echo " " . $obj->cidade . " - " . $obj->estado . ", "; }			
														if ($obj->idade != "Não sei"){
															
																echo " " . $obj->idade . ', ';
																
															
														}								
														?>
														<?php								
														if ($obj->id_racas > 0){
															echo '#raca';
														}								
														?>
														<?php								
														if ($obj->sexo != "Não sei"){
															echo '#'. $obj->sexo;
														}								
														?>
														<?php								
														if ($obj->pedigree == "Sim"){
															echo '#pedigree';
														}								
														?>														
														<?php								
														if ($obj->castrado == "Sim"){
															if ($obj->sexo == "Fêmea"){											
																echo " #castrada";
															} else {
																echo " #castrado";
															}
														}								
														?>
														<?php								
														if ($obj->vacinado == "Sim"){
															if ($obj->sexo == "Fêmea"){										
																echo " #vacinada";
															} else {
																echo " #vacinado";
															}
															
															
														}								
														?>
														<?php								
														if ($obj->vermifugado == "Sim"){
															if ($obj->sexo == "Fêmea"){										
																echo " #vermifugada";
															} else {
																echo " #vermifugado";
															}
																
															
														}								
														?>
														
														</figcaption>	
														</a>		
														</figure>
													</div>
								
												<?php
												//}
											}
											?>		
											
						</div>



					</div>




                <?php
                require("../inc/rodape.php");
                ?>



                
              </div>
			
			
            
            
            </div>
         
       
                       
              
                     
              
              
           </div>
         

 
  <!--   Core JS Files   -->
  <?php
 require("../inc/scripts.php") ;
  ?>
  <script>
	function cidades(){

		$("#divCidades").load("cidades.php?estado="+$("#uf").val());
		
	}
	function filtrar(){
		$("#frmFiltro").action = "index.php";
		$("#frmFiltro").submit();

	}

	$('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
	  var url = $(this).attr("href"); // the remote url for content
	  var target = $(this).data("target"); // the target pane
	  var tab = $(this); // this tab
	  
	  // ajax load from data-url
	  $(target).load(url,function(result){      
		tab.tab('show');
	  });
	});

	// initially activate the first tab..
	//$('#tabIni').click();  
  </script>  
</body>

</html>

<script>

</script>
